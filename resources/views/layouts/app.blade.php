<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Poppins:wght@500&display=swap" rel="stylesheet">
    <title> @yield('title') | {{ env('APP_NAME') }} </title>
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/assets/images/favicon.png') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.1/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
    @yield('header')
    <style>
        .error_msg{
            color:red
        }
        .loginFormSec .accountCol input {
            width:100%;
        }
        .forgetPass {
            float: right;
            margin-top: 15px;
        }
    </style>
</head>

<body>
    <main>
        @include('layouts.app-nav')
        @yield('banner')
        @yield('content')

        @include('layouts.footer')
    </main>
    <script>
			var path = "{{ url('') }}/";
			var currentUrl = "{{ Request::url() }}";
			var secondParameter = "{{ Request::segment(2) }}";
            var asset = "{{ asset('') }}/";
            var is_auth = "{{ Auth::check() ? true : false }}";
            var auth_user = "{{ Auth::check() ? Auth::user() : null }}";
            var auth_id = "{{ Auth::check() ? Auth::id() : null }}";
        </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
    <script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
    @yield('footer')

    <script>
        $('.add-cart').click(function(e){
        e.preventDefault()
		let id = $(this).attr('data-id')
		let quant = parseInt($('#counter-value').val()) ? $('#counter-value').val() : 1
		var formData = new FormData();
		formData.append('id',id)
		formData.append('quant',quant)
		$.ajaxSetup({
        	headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    	})
		$.ajax({
			contentType: false,
			cache: false,
			processData: false,
			url: `{{ url('/cart') }}`,
			method: 'POST',
			data: formData,
			success:function(res){
				if(res.msg == 'success'){
					$.notify('Product Added To Cart', { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
					getCartCount()
				}
				else{
					$.notify('Unable To Add Product To Cart', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
				}
			}
		});
    })
    getCartCount()
    function getCartCount(){
        $.ajax({
            url:"{{ url('get-cart-count') }}",
            type:"GET",
            success:function(res){
                $('#cartCount').html(res.cartCount) 
            }
        })
    }
    </script>
</body>

</html>