<header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="logo-box">
                            <a href="/">
                                <img src="{{ asset('/assets/images/logo.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="nav-box">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">New Vehicles</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Pre-Owned Vehicles</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="true" aria-expanded="false">Packages</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ url('/package/automotive') }}">Automotive Dealer</a>
                                        <a class="dropdown-item" href="{{ url('/package/private') }}">Private Seller</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/products') }}">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Testimonials</a>
                                </li>
                                <!-- <li class="nav-item">
                                        <a class="nav-link" href="#">FAQ</a>
                                    </li> -->
                                @if(Auth::check())
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Contact Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/logout') }}">Logout</a>
                                    </li>
                                @else
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact Us</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                                </li>

                                @endif

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i> <span class="badge badge-light" id="cartCount">4</span></a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="right-side-nav">
                            <p>
                                <label>
                                    @if(Auth::check())
                                        Hi! {{ Auth::user()->name }}
                                    @else
                                        List Your Accessorized Vehicle
                                    @endif
                                </label>
                                @if(Auth::check())
                                    @if(Auth::user()->user_type == 'Admin')
                                    <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                                    @elseif(Auth::user()->user_type == 'automotive')
                                    <a href="{{ url('automotive/dashboard') }}">Dashboard</a>
                                    @elseif(Auth::user()->user_type == 'private')
                                    <a href="{{ url('private/dashboard') }}">Dashboard</a>
                                    @else
                                    <a href="{{ url('/user/dashboard') }}">Dashboard</a>
                                    @endif
                                @else
                                    <a href="{{ url('/selling-profile') }}">
                                        Click Here
                                    </a>
                                @endif
                                
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </header>