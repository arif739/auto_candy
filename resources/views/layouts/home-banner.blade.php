<section class="hero">
            <div class="container">
                <div class="heading">
                    <h3>
                        FIND YOUR DREAM VEHICLE HERE
                    </h3>
                </div>
                <div class="filter-main-box">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="heading">
                                <h4>
                                    Find Accessorized Vehicles
                                    For Sale on Autocandy
                                </h4>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="filter-fields">
                                <div class="field-box select">
                                    <select name="make">
                                        <option value="" disabled selected>Browse By Make</option>
                                        <option value="make-1">Make 1</option>
                                        <option value="make-2">Make 2</option>
                                        <option value="make-3">Make 3</option>
                                        <option value="make-4">Make 4</option>
                                    </select>
                                </div>
                                <div class="field-box select">
                                    <select name="model">
                                        <option value="" disabled selected>Browse By Model</option>
                                        <option value="model-1">Model 1</option>
                                        <option value="model-2">Model 2</option>
                                        <option value="model-3">Model 3</option>
                                        <option value="model-4">Model 4</option>
                                    </select>
                                </div>
                                <div class="field-box">
                                    <input type="text" name="zip" placeholder="Zip Code" max-length="3">
                                </div>
                                <div class="field-box select">
                                    <select name="price">
                                        <option value="" disabled selected>Price Range</option>
                                        <option value="range-1">Range 1</option>
                                        <option value="range-2">Range 2</option>
                                        <option value="range-3">Range 3</option>
                                        <option value="range-4">Range 4</option>
                                    </select>
                                </div>
                                <div class="field-box">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="advance-cta">
                                <a href="#">
                                    Advanced Search <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>