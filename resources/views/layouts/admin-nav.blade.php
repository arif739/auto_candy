<div class="header-bottom">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Header Menu Wrapper-->
								<div class="header-navs header-navs-left" id="kt_header_navs">
									<!--begin::Tab Navs(for tablet and mobile modes)-->
									<ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean active" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">Reports</a>
										</li>
										<!--end::Item-->
									</ul>
									<!--begin::Tab Navs-->
									<!--begin::Tab Content-->
									<div class="tab-content">
										<!--begin::Tab Pane-->
										<div class="tab-pane py-5 p-lg-0 show active" id="kt_header_tab_1">
											<!--begin::Menu-->
											<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
												<!--begin::Nav-->
												<ul class="menu-nav">

													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/home') }}" class="menu-link">
															<span class="menu-text">Home</span>
														</a>
                                                    </li>

													@if(Auth::user()->user_type == 'Admin')
													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/admin/dashboard') }}" class="menu-link">
															<span class="menu-text">Dashboard</span>
														</a>
                                                    </li>

                                                    <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Packages</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/packages/automotive') }}" class="menu-link">
																		<span class="menu-text">Automotive Dealer Packages</span>
																	</a>
                                                                </li>
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/packages/private') }}" class="menu-link">
																		<span class="menu-text">Private Seller Packages</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>

													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/admin/coupon') }}" class="menu-link">
															<span class="menu-text">Coupon</span>
														</a>
													</li>

													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/car-option') }}" class="menu-link">
															<span class="menu-text">Car Options</span>
														</a>
													</li>
													
													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/admin/orders') }}" class="menu-link">
															<span class="menu-text">Purchased Packages</span>
														</a>
													</li>
													
													<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Shop</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/product') }}" class="menu-link">
																		<span class="menu-text">Product</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/categories') }}" class="menu-link">
																		<span class="menu-text">Categories</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/sub-categories') }}" class="menu-link">
																		<span class="menu-text">Sub Categories</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>

													<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Shop Orders</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/order/pending') }}" class="menu-link">
																		<span class="menu-text">Pending</span>
																	</a>
                                                                </li>
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/order/processing') }}" class="menu-link">
																		<span class="menu-text">Processing</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/order/completed') }}" class="menu-link">
																		<span class="menu-text">Completed</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/admin/order/cancel') }}" class="menu-link">
																		<span class="menu-text">Cancel</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>

													@elseif(Auth::user()->user_type == 'automotive')
													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/automotive/dashboard') }}" class="menu-link">
															<span class="menu-text">Dashboard</span>
														</a>
													</li>
													
													<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Orders</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/pending') }}" class="menu-link">
																		<span class="menu-text">Pending</span>
																	</a>
                                                                </li>
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/processing') }}" class="menu-link">
																		<span class="menu-text">Processing</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/completed') }}" class="menu-link">
																		<span class="menu-text">Completed</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/cancel') }}" class="menu-link">
																		<span class="menu-text">Cancel</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>

													@elseif(Auth::user()->user_type == 'private')
													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/private/dashboard') }}" class="menu-link">
															<span class="menu-text">Dashboard</span>
														</a>
													</li>
													
													<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Orders</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/pending') }}" class="menu-link">
																		<span class="menu-text">Pending</span>
																	</a>
                                                                </li>
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/processing') }}" class="menu-link">
																		<span class="menu-text">Processing</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/completed') }}" class="menu-link">
																		<span class="menu-text">Completed</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/cancel') }}" class="menu-link">
																		<span class="menu-text">Cancel</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													@elseif(Auth::user()->user_type == 'User')
													<li class="menu-item" aria-haspopup="true">
														<a href="{{ url('/user/dashboard') }}" class="menu-link">
															<span class="menu-text">Dashboard</span>
														</a>
													</li>
													
													<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Orders</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/pending') }}" class="menu-link">
																		<span class="menu-text">Pending</span>
																	</a>
                                                                </li>
                                                                
                                                                <li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/processing') }}" class="menu-link">
																		<span class="menu-text">Processing</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/completed') }}" class="menu-link">
																		<span class="menu-text">Completed</span>
																	</a>
																</li>

																<li class="menu-item" aria-haspopup="true">
																	<a href="{{ url('/user-order/cancel') }}" class="menu-link">
																		<span class="menu-text">Cancel</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													@endif
													<!-- <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Features</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="javascript:;" class="menu-link menu-toggle">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M22,17 L22,21 C22,22.1045695 21.1045695,23 20,23 L4,23 C2.8954305,23 2,22.1045695 2,21 L2,17 L6.27924078,17 L6.82339262,18.6324555 C7.09562072,19.4491398 7.8598984,20 8.72075922,20 L15.381966,20 C16.1395101,20 16.8320364,19.5719952 17.1708204,18.8944272 L18.118034,17 L22,17 Z" fill="#000000" />
																					<path d="M2.5625,15 L5.92654389,9.01947752 C6.2807805,8.38972356 6.94714834,8 7.66969497,8 L16.330305,8 C17.0528517,8 17.7192195,8.38972356 18.0734561,9.01947752 L21.4375,15 L18.118034,15 C17.3604899,15 16.6679636,15.4280048 16.3291796,16.1055728 L15.381966,18 L8.72075922,18 L8.17660738,16.3675445 C7.90437928,15.5508602 7.1401016,15 6.27924078,15 L2.5625,15 Z" fill="#000000" opacity="0.3" />
																					<path d="M11.1288761,0.733697713 L11.1288761,2.69017121 L9.12120481,2.69017121 C8.84506244,2.69017121 8.62120481,2.91402884 8.62120481,3.19017121 L8.62120481,4.21346991 C8.62120481,4.48961229 8.84506244,4.71346991 9.12120481,4.71346991 L11.1288761,4.71346991 L11.1288761,6.66994341 C11.1288761,6.94608579 11.3527337,7.16994341 11.6288761,7.16994341 C11.7471877,7.16994341 11.8616664,7.12798964 11.951961,7.05154023 L15.4576222,4.08341738 C15.6683723,3.90498251 15.6945689,3.58948575 15.5161341,3.37873564 C15.4982803,3.35764848 15.4787093,3.33807751 15.4576222,3.32022374 L11.951961,0.352100892 C11.7412109,0.173666017 11.4257142,0.199862688 11.2472793,0.410612793 C11.1708299,0.500907473 11.1288761,0.615386087 11.1288761,0.733697713 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 3.661508) rotate(-270.000000) translate(-11.959697, -3.661508)" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">Maps</span>
																		<i class="menu-arrow"></i>
																	</a>
																	<div class="menu-submenu menu-submenu-classic menu-submenu-right">
																		<ul class="menu-subnav">
																			<li class="menu-item" aria-haspopup="true">
																				<a href="features/maps/google-maps.html" class="menu-link">
																					<i class="menu-bullet menu-bullet-dot">
																						<span></span>
																					</i>
																					<span class="menu-text">Google Maps</span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</li>
																<li class="menu-item" aria-haspopup="true">
																	<a target="_blank" href="https://keenthemes.com/metronic/preview/demo7/builder.html" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
																					<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519)" x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">Layout Builder</span>
																	</a>
																</li>
															</ul>
														</div>
                                                    </li> -->
                                                    

												</ul>
												<!--end::Nav-->
											</div>
											<!--end::Menu-->
										</div>
										<!--begin::Tab Pane-->
										<!--begin::Tab Pane-->
										<div class="tab-pane p-5 p-lg-0 justify-content-between" id="kt_header_tab_2">
											<div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
												<!--begin::Actions-->
												<a href="#" class="btn btn-light-success font-weight-bold mr-3 my-2 my-lg-0">Latest Orders</a>
												<a href="#" class="btn btn-light-primary font-weight-bold my-2 my-lg-0">Customer Service</a>
												<!--end::Actions-->
											</div>
											<div class="d-flex align-items-center">
												<!--begin::Actions-->
												<a href="#" class="btn btn-danger font-weight-bold my-2 my-lg-0">Generate Reports</a>
												<!--end::Actions-->
											</div>
										</div>
										<!--begin::Tab Pane-->
									</div>
									<!--end::Tab Content-->
								</div>
								<!--end::Header Menu Wrapper-->
							</div>
							<!--end::Container-->
						</div>