<footer class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-logo">
                                <img src="{{ asset('/assets/images/logo.png') }}" alt="">
                            </div>
                            <div class="footer-about-desc">
                                <p>
                                    This is dummy copy. It is not meant to be read. It has been placed here solely to
                                    demonstrate the look and feel of finished, typeset text. Only for show. He who
                                    searches
                                    for meaning here..[.]
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="footer-links">
                                <div class="heading">
                                    <h3>
                                        Useful Links
                                    </h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    Home
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    About Us
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    New Vehicles
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Pre-Owned Vehicles
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    Packages
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Blog
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Testimonials
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    FAQ
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    Contact Us
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Terms & Conditions
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Privacy Policy
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Admin Login
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    Seller Login
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Buyer Login
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="heading">
                                <h3>
                                    Explore AutoCandy
                                </h3>
                            </div>
                            <div class="bottom-links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            New Vehicles
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Sitemap
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Pre-Owned Vehicles
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="footer-bottom-right">
                                <div class="heading">
                                    <h3>
                                        Newsletter
                                    </h3>
                                </div>
                                <div class="desc">
                                    <p>
                                        Sign up for our mailing list to get latest updates and offers
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <form>
                                            <div class="field-box">
                                                <input type="email" placeholder="Enter your Email Address">
                                            </div>
                                            <div class="btn-box">
                                                <button type="submit">GO</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="social-links">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fab fa-pinterest"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fab fa-instagram"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <p>
                                © <?php echo date("Y"); ?>, <a href="/">AutoCandy</a>. All Rights Reserved. Designed &
                                Developed
                                by Dallas Web Design Company <a target="_blank"
                                    href="https://www.invictusstudio.com/">Invictus
                                    Studio.</a>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="card-img">
                                <img src="{{ asset('/assets/images/cards.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>