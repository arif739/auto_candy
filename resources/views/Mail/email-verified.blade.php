<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env('APP_NAME') }}</title>
</head>
<body>

<h3>Hi {{ $data['name'] }}!</h3>
<p>{{ $data['msg'] }}</p>
<a href="{{ $data['url'] }}">Click Here</a>
<h5>Thank You.</h5>
    
</body>
</html>