@extends('layouts.app')

@section('title') Update Password @endsection

@section('header')
    
@endsection

@section('banner')
    @include('layouts.banner',['title' => 'Update Password'])
@endsection

@section('content')
    

<section class="loginFormSec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert') }}" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6 accountCol">
                <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info" >
                        <div class="panel-heading">
                            <h2 class="dynamicHeading">Update Password</h2>
                        </div>
                        <div style="padding-top:30px" class="panel-body" >
                            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            <form id="loginform" class="form-horizontal" role="form">
                                <div  class="input-group loginGroup">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <label for="email" class="control-label col-md-12 secondaryHeading">Password</label>
                                    <input id="password" type="password" class="form-control col-md-12" name="password" placeholder="********">
                                    <small class="error_msg" id="password_msg"></small>
                                </div>                                        
                                <div  class="input-group loginGroup">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <label for="email" class="control-label col-md-12 secondaryHeading">Confirm Password</label>
                                    <input id="password_confirmation" type="password" class="form-control col-md-12" name="password_confirmation" placeholder="********">
                                    <small class="error_msg" id="password_confirmation_msg"></small>
                                </div>

                                {{--  <div class="input-group">
                                    <div class="checkbox">
                                        <label>
                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                    </div>
                                </div>  --}}
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls pl-0">
                                        <button id="btnSub" type="button" class="btn  getBtn ml-0">Update Password</button>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('footer')


<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>

<script>
$('#btnSub').click(function(e){
    e.preventDefault()
    $('.error_msg').html('')
    let password = $('#password').val();
    let cpassword = $('#password_confirmation').val();
    if(password == ''){
        $('#password_msg').html('Password field is required')
    }
    else if(cpassword == ''){
        $('#password_confirmation_msg').html('Confirm password field is required')
    }
    else if(cpassword !== password){
        $.notify('Both Passwords Field Should Be Same', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
    }
    else{
        var formData = new FormData();
        formData.append('password',password)
        formData.append('password_confirmation',cpassword)
        formData.append('email',"{{$email}}")
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:`{{ url('/password/confirm') }}`,
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
                $("#btnSub").prop("disabled", true);
            },
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    setTimeout(() => {
                        window.location.href = res.url
                    }, 1500);
                }
                else{
                    $.notify('Error while updating password', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
                $('#email').val('')
                $("#btnSub").prop("disabled", false);
            },
            error:function(error){
                $.each(error.responseJSON.errors,function(key,value) {
                    $("#"+key+"_msg").html(value);
                });
            $("#btnSub").prop("disabled", false);
            },
        })
    }
})
</script>

@endsection
