@extends('layouts.admin')

@section('title') Sub Categories @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Sub Categories', 'type' => 'option', 'btn' => 'Create Sub Category'  ])
@endsection

@section('content')

<div class="section-body mt-3">
    <div class="container-fluid">

        <div class="row clearfix mt-2">

            <div class="col-lg-12" id="post" style="display:none">
            <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Sub Category</h3>
                        <div class="card-toolbar">
                            <div class="example-tools justify-content-center">
                            <button type="button" class="btn btn-danger btn-sm" id="close"><span class="fa fa-times"></span></button>
                        </div>
                        </div>
                    </div>
                    <form id="formSubmit">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Category Name" />
                                    <small class="error_msg" id="name_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="0" hidden selected>Select Category</option>
                                        @foreach($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                    <small class="error_msg" id="category_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control" placeholder="Category Description"></textarea>
                                    <small class="error_msg" id="description_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" />
                                    <small class="error_msg" id="image_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>
                            <input type="hidden" name="type" value="product" />
                            <div class="col-md-6 imgDiv images" style="display:none"></div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="col-md-12">
                            <button type="submit" id="btnSub" data-type="create" class="btn btn-primary mb-2" style="float:right">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>

            <div class="col-lg-12" id="details">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>



<script>

$('#formSubmit').on('submit',function(e){
    e.preventDefault()
    $('.error_msg').html('')
    var btnSub = $("#btnSub").attr('data-type')
    const formData = new FormData(this)
    var url = btnSub == 'create' ? "{{route('sub-categories.store')}}" : `{{url('/admin/sub-categories')}}/${$('#id').val()}`
    btnSub == 'update' && formData.append('_method','PUT')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        contentType: false,
        cache: false,
        processData: false,
        url: url,
        method: 'POST',
        data: formData,
        beforeSend:function(){
            $("#btnSub").prop("disabled", true);
        },
        success:function(res){
            if(res.msg == 'success'){
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                close()
                $('#btnSub').attr('data-type','create')
                getData()
            }
            else{
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                $('#btnSub').attr('data-type','create')
            }
            $("#btnSub").prop("disabled", false);
        },
        error:function(error){
            $.each(error.responseJSON.errors,function(key,value) {
                $("#"+key+"_msg").html(value);
            });
                $("#btnSub").prop("disabled", false);
        },
    })

})




$(document).on('click','.edit',function(){
    $('.error_msg').html('')
    var id = $(this).attr('data-id')
    $.ajax({
        url:`{{url('/admin/sub-categories')}}/${id}/edit`,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
                var html = ''
                $('#name').val(res.post.name)
                $('#id').val(res.post.id)
                $('#description').val(res.post.description)
                $(`#category option[value=${res.post.category}]`).attr('selected','selected');
                $('#img').attr('src',res.post.image)
                $('#img').attr('href',res.post.image)
                $('#btnSub').attr('data-type','update')
                $('.imgDiv').css('display','block')
                $('.title').html('Update Category')
                open()
                window.scrollTo({ top: 100, left: 100, behavior: 'smooth' });
            }
            else{
                $.notify('Unable to fetch record.', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
            }
        }
    })
})


$(document).on('click','.delImage',function(){
    var id = $(this).attr('data-id')
    $.ajax({
        url:`{{url('/admin/sub-categories/create')}}?id=${id}`,
        type:"GET",
        success:function(res){
            if(res == '1'){
                $('#'+id).remove()
            }
        }
    })
})


$(document).on('click','.delete',function(){
    var id = $(this).attr('data-id')
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            url:`{{url('admin/sub-categories')}}/${id}`,
            type:"DELETE",
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    getData()
                }
                else{
                    $.notify('Error while deleting event', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
            }
        })

        }
    })

})

getData()

function getData(){
    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
          ajax:{
              url: "{{ route('sub-categories.index') }}"
           },
          columns: [
            { data: 'image', name: 'image', orderable: false },
            { data: 'name', name: 'name' },
            { data: 'category', name: 'category' },
            { data: 'description', name: 'description' },
            { data: 'date', name: 'date' },
            { data: 'action', name: 'action', orderable: false },
        ]

    });
}

$(document).on('click','.showMore',function(){
    let id = $(this).attr('data-id')
    let type = $(this).attr('data-type')
    let less = $(this).attr('data-less')
    let more = $(this).attr('data-content')
    if(type == 'less'){
        $(`#${id}`).attr('data-type','more')
        $(`#content${id}`).html(more)
        $(`#${id}`).html('[See Less]')
    }
    else if(type == 'more'){
        $(`#${id}`).attr('data-type','less')
        $(`#content${id}`).html(less)
        $(`#${id}`).html('[See More]')
    }
})

</script>
    
@endsection