@extends('layouts.admin')

@section('title') Car Option @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Car Options', 'type' => 'option', 'btn' => 'Create Option'  ])
@endsection

@section('content')


<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact" id="post" style="display:none">
    <div class="card-header">
        <h3 class="card-title">Options</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" id="close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form id="formSubmit">
        <div class="card-body">
            <div class="row">

            <div class="col-md-6">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" placeholder="Option Name" name="name" id="name" required />
                <span class="form-text text-muted error_msg" id="name_msg"></span>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label>Option Type</label>
                <select class="form-control" id="type" name="type"required >
                <option value="0" selected hidden>Select Option Type</option>
                    <!-- <option value="Miles">Miles</option> -->
                    <option value="1">All</option>
                    <option value="Make">Make</option>
                    <option value="Body Style">Body Style</option>
                    <option value="Price Range">Price Range</option>
                    <!-- <option value="Year Range">Year Range</option> -->
                    <option value="Condition">Condition</option>
                    <option value="Interior Color">Interior Color</option>
                    <option value="Exterior Color">Exterior Color</option>
                    <option value="Transmission">Transmission</option>
                    <option value="Drivetrain">Drivetrain</option>
                    <option value="Cylinder Count">Cylinder Count</option>
                    <option value="Fuel">Fuel</option>
                    <option value="Exterior">Exterior</option>
                    <option value="Interior">Interior</option>
                    <option value="Lift Kits">Lift Kits</option>
                    <option value="Lightning">Lightning</option>
                    <option value="Performance">Performance</option>
                    <option value="Wheels">Wheels</option>
                </select>
                <span class="form-text text-muted error_msg" id="type_msg"></span>
            </div>
            </div>

            <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>

            <div class="col-md-6 imgDiv images" style="display:none"></div>

            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mb-2" id="btnSub" data-type="create" style="float:right">Submit</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->



<!--begin::Card-->
<div class="card card-custom" id="details">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label" style="text-transform:capitalize">Manage Car Options</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
                <select class="form-control" id="filter">
                    <option value="0" selected hidden>Select Option Type</option>
                    <!-- <option value="Miles">Miles</option> -->
                    <option value="1">All</option>
                    <option value="Make">Make</option>
                    <option value="Body Style">Body Style</option>
                    <option value="Price Range">Price Range</option>
                    <!-- <option value="Year Range">Year Range</option> -->
                    <option value="Condition">Condition</option>
                    <option value="Interior Color">Interior Color</option>
                    <option value="Exterior Color">Exterior Color</option>
                    <option value="Transmission">Transmission</option>
                    <option value="Drivetrain">Drivetrain</option>
                    <option value="Cylinder Count">Cylinder Count</option>
                    <option value="Fuel">Fuel</option>
                    <option value="Exterior">Exterior</option>
                    <option value="Interior">Interior</option>
                    <option value="Lift Kits">Lift Kits</option>
                    <option value="Lightning">Lightning</option>
                    <option value="Performance">Performance</option>
                    <option value="Wheels">Wheels</option>
                </select>
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover table-checkable" id="datatable" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <!-- <th>Order</th> -->
                    <th>Name</th>
                    <th>Type</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script src="{{ asset('/script/caroption.js') }}"></script>

@endsection