@extends('layouts.admin')

@section('title') Product Details @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Product Details', 'type' => null, 'btn' => 'Create Product'  ])
@endsection

@section('content')

<div class="section-body mt-3">
    <div class="container-fluid">

        <div class="d-flex justify-content-between align-items-center">
            <ul class="nav nav-tabs page-header-tab">
                {{--  <li class="nav-item"><a class="nav-link" href="table-normal.html">Basic</a></li>  --}}
            </ul>
            <div class="header-action">
            </div>
        </div>

        <div class="row clearfix mt-2">

            <div class="col-lg-12" id="details">
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5 text-center">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                            <img href="{{ $product->image }}" data-fancybox="images" data-caption="{{ $product->name }}" class="fancy-box" src="{{ $product->image }}" style="height:300px; width:100%" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <th>{{ $product->name }}</th>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td>$ {{ $product->price }}</td>
                                    </tr>
                                    <tr>
                                        <td>Category</td>
                                        <td>{{ $product->categories->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sub Category</td>
                                        <td>{{ $product->subCategories->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Quantity</td>
                                        <td>{{ $product->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Minimum Quantity</td>
                                        <td>{{ $product->min_quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Max Quantity</td>
                                        <td>{{ $product->max_quantity }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Short Description</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">{{ $product->short_description }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Description</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">{{ $product->description }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Product Info</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">{{ $product->product_info }}</td>
                                    </tr>
                                </tbody>
                                <tfooter>
                                    <tr>
                                        <th colspan="2">Product Images</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                @foreach($images as $img)
                                                <div class="col-md-3">
                                                <img href="{{ asset('/uploads/images/'.$img->image) }}" data-fancybox="images" data-caption="{{ $product->name }}" class="fancy-box" src="{{ asset('/uploads/images/'.$img->image) }}" style="height:200px; width:100%; border-radius:10px" />
                                                </div>
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                </tfooter>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>



@endsection

@section('footer')
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
@endsection