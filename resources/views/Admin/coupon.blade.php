@extends('layouts.admin')

@section('title') Coupon @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Coupon', 'type' => 'packages', 'btn' => 'Create Coupon'  ])
@endsection

@section('content')


<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact" id="post" style="display:none">
    <div class="card-header">
        <h3 class="card-title">Coupon</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" id="close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form id="formSubmit">
        <div class="card-body">
            <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" id="title" class="form-control" placeholder="Coupon Title" />
                    <span class="form-text text-muted error_msg" id="title_msg"></span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Expiry Date</label>
                    <input type="date" name="expiry_date" id="expiry_date" class="form-control" />
                    <span class="form-text text-muted error_msg" id="expiry_date_msg"></span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Coupon Type</label>
                    <select name="coupon_type" id="coupon_type" class="form-control">
                        <option value="0" selected hidden>Select Coupon Type</option>
                        <option value="Percent">Percent</option>
                        <option value="Amount">Amount</option>
                    </select>
                    <span class="form-text text-muted error_msg" id="coupon_type_msg"></span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Value</label>
                    <input type="number" step="any" name="value" id="value" class="form-control" placeholder="Discount Value e.g. 10"/>
                    <span class="form-text text-muted error_msg" id="value_msg"></span>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label>Coupon Code</label>
                    <input type="text" name="code" id="code" class="form-control" placeholder="AC123"/>
                    <span class="form-text text-muted error_msg" id="code_msg"></span>
                </div>
            </div>

            <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>

            <div class="col-md-6 imgDiv images" style="display:none"></div>
            <input type="hidden" value="product" name="product" />
            <input type="hidden" name="type" value="product" />
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mb-2" id="btnSub" data-type="create" style="float:right">Submit</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->



<!--begin::Card-->
<div class="card card-custom" id="details">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label" style="text-transform:capitalize">Manage Coupons</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">

            </div>
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover table-checkable" id="datatable" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <!-- <th>Order</th> -->
                    <th>Title</th>
                    <th>Expiry Date</th>
                    <th>Code</th>
                    <th>Type</th>
                    <th>Value</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>

$('#formSubmit').on('submit',function(e){
    e.preventDefault()
    $('.error_msg').html('')
    var btnSub = $("#btnSub").attr('data-type')
    const formData = new FormData(this)
    var url = btnSub == 'create' ? "{{route('coupon.store')}}" : `{{url('/admin/coupon')}}/${$('#id').val()}`
    btnSub == 'update' && formData.append('_method','PUT')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        contentType: false,
        cache: false,
        processData: false,
        url: url,
        method: 'POST',
        data: formData,
        beforeSend:function(){
            $("#btnSub").prop("disabled", true);
        },
        success:function(res){
            if(res.msg == 'success'){
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                close()
                $('#btnSub').attr('data-type','create')
                getData()
            }
            else{
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                $('#btnSub').attr('data-type','create')
            }
            $("#btnSub").prop("disabled", false);
        },
        error:function(error){
            $.each(error.responseJSON.errors,function(key,value) {
                $("#"+key+"_msg").html(value);
            });
                $("#btnSub").prop("disabled", false);
        },
    })

})




$(document).on('click','.edit',function(){
    $('.error_msg').html('')
    var id = $(this).attr('data-id')
    $.ajax({
        url:`{{url('/admin/coupon')}}/${id}/edit`,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
                var html = ''
                $('#title').val(res.post.title)
                $('#code').val(res.post.code)
                $('#id').val(res.post.id)
                $('#value').val(res.post.value)
                $('#expiry_date').val(res.post.expiry_date)
                $(`#coupon_type option[value=${res.post.coupon_type}]`).attr('selected','selected')
                $('#btnSub').attr('data-type','update')
                $('.title-head').html('Update Coupon')
                open()
            }
            else{
                $.notify('Unable to fetch record.', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
            }
        }
    })
})



$(document).on('click','.delete',function(){
    var id = $(this).attr('data-id')
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            url:`{{url('admin/coupon')}}/${id}`,
            type:"DELETE",
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    getData()
                }
                else{
                    $.notify('Error while deleting event', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
            }
        })

        }
    })

})

getData()

function getData(){
    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
          ajax:{
              url: "{{ route('coupon.index') }}"
           },
          columns: [
            { data: 'title', name: 'title' },
            { data: 'expire', name: 'expire' },
            { data: 'code', name: 'code' },
            { data: 'type', name: 'type' },
            { data: 'value', name: 'value' },
            { data: 'action', name: 'action', orderable: false },
        ]

    });
}

</script>

@endsection