@extends('layouts.admin')

@section('title') Order Details @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Order Details', 'type' => null, 'btn' => 'Create Product'  ])
@endsection

@section('content')

<div class="section-body mt-3">
    <div class="container-fluid">

        <div class="d-flex justify-content-between align-items-center">
            <ul class="nav nav-tabs page-header-tab">
                {{--  <li class="nav-item"><a class="nav-link" href="table-normal.html">Basic</a></li>  --}}
            </ul>
            <div class="header-action">
            </div>
        </div>

        <div class="row clearfix mt-2">

            <div class="col-lg-4" id="details">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $order->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $order->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>{{ $order->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address 2</td>
                                        <td>{{ $order->address2 }}</td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td>{{ $order->country }}</td>
                                    </tr>
                                    <tr>
                                        <td>State</td>
                                        <td>{{ $order->state }}</td>
                                    </tr>
                                    <tr>
                                        <td>Zip</td>
                                        <td>{{ $order->zip }}</td>
                                    </tr>
                                    @if(!$order->is_coupon)
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ round($order->total,2) }}</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <th>Coupon Discount</th>
                                        <th>{{ $order->coupon_type == 'Percent' ? $order->coupon->value.'%' : '$'.$order->coupon->value }}</th>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <th>{{ round($order->total,2) }}</th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
            
            <div class="col-lg-8">
            <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td><a href="{{ url('/admin/product/'.Crypt::encrypt($product->product->id)) }}">{{ $product->product->name }}</a></td>
                                        <td>{{ $product->quantity }}x</td>
                                        <td>${{ round($product->total,2) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>



@endsection

@section('footer')
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
@endsection