@extends('layouts.admin')

@section('title') Packages @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => Request::segment(2).' Packages', 'type' => 'packages', 'btn' => 'Create Package'  ])
@endsection

@section('content')


<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact" id="post" style="display:none">
    <div class="card-header">
        <h3 class="card-title">Package</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" id="close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form id="formSubmit">
        <div class="card-body">
            <div class="row">

            <div class="col-md-6">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" placeholder="Basic Package" name="title" id="title" required />
                <span class="form-text text-muted error_msg" id="title_msg"></span>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label>Duration</label>
                <select class="form-control" id="duration" name="duration"required >
                    <option value="0" selected hidden>Select Duration</option>
                    <option value="1">One Time Only</option>
                    <option value="2">Once Per Month</option>
                </select>
                <span class="form-text text-muted error_msg" id="duration_msg"></span>
            </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label>Package Type</label>
                    <select class="form-control" id="package_type" name="package_type" required >
                    <option value="0" selected hidden>Select Package Type</option>
                    <option value="Paid">Paid</option>
                    <option value="Free Trial">Free Trial</option>
                    </select>
                    <span class="form-text text-muted error_msg" id="package_type_msg"></span>
                </div>
            </div>

            <div class="col-md-6" id="priceBox" style="display:none">
            <div class="form-group">
                <label>Price</label>
                <input type="number" step="any" min="1" class="form-control" placeholder="100.00" name="price" id="price" />
                <span class="form-text text-muted error_msg" id="price_msg"></span>
            </div>
            </div>

            <div class="col-md-6" id="trialBox" style="display:none">
            <div class="form-group">
                <label>Trial Days</label>
                <input type="number" step="any" min="1" class="form-control" placeholder="90" name="trial_days" id="trial_days" />
                <span class="form-text text-muted error_msg" id="trial_days_msg"></span>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label>Vehicle Allowed</label>
                <input type="number" min="1" class="form-control" placeholder="10" name="vehicle" id="vehicle" required />
                <span class="form-text text-muted error_msg" id="vehicle_msg"></span>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label>Picture Allowed</label>
                <input type="number" min="1" class="form-control" placeholder="20" name="picture" id="picture" required />
                <span class="form-text text-muted error_msg" id="picture_msg"></span>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label>Video Allowed</label>
                <input type="number" min="0" class="form-control" placeholder="1" name="video" id="video" required />
                <span class="form-text text-muted error_msg" id="video_msg"></span>
            </div>
            </div>

            <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>

            <div class="col-md-6 imgDiv images" style="display:none"></div>

            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mb-2" id="btnSub" data-type="create" style="float:right">Submit</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->



<!--begin::Card-->
<div class="card card-custom" id="details">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label" style="text-transform:capitalize">Manage {{ Request::segment(2) }} Packages</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">

            </div>
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover table-checkable" id="datatable" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <!-- <th>Order</th> -->
                    <th>Title</th>
                    <th>Duration</th>
                    <th>Package Type</th>
                    <th>Price</th>
                    <th>Trial Days</th>
                    <th>Vehicle</th>
                    <th>Image</th>
                    <th>Video</th>
                    <!-- <th>Created</th> -->
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script src="{{ asset('/script/packages.js') }}"></script>

@endsection