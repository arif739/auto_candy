@extends('layouts.admin')

@section('title') Products @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Products', 'type' => 'option', 'btn' => 'Create Product'  ])
@endsection

@section('content')

<div class="section-body mt-3">
    <div class="container-fluid">

        <div class="row clearfix mt-2">

            <div class="col-lg-12" id="post" style="display:none">
            <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Product</h3>
                        <div class="card-toolbar">
                            <div class="example-tools justify-content-center">
                            <button type="button" class="btn btn-danger btn-sm" id="close"><span class="fa fa-times"></span></button>
                        </div>
                        </div>
                    </div>
                    <form id="formSubmit">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product Name" />
                                    <small class="error_msg" id="name_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="number" name="price" id="price" class="form-control" placeholder="100.00" step="any" />
                                    <small class="error_msg" id="price_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="0" hidden selected>Select Category</option>
                                        @foreach($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                    <small class="error_msg" id="category_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sub Category</label>
                                    <select name="sub_category" id="sub_category" class="form-control">
                                        <option value="0" hidden selected>Select Sub Category</option>
                                    </select>
                                    <small class="error_msg" id="sub_category_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" id="quantity" class="form-control" placeholder="100" min="1" />
                                    <small class="error_msg" id="quantity_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Min Order Quantity</label>
                                    <input type="number" name="min_quantity" id="min_quantity" class="form-control" placeholder="10" min="1" />
                                    <small class="error_msg" id="min_quantity_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Max Order Quantity</label>
                                    <input type="number" name="max_quantity" id="max_quantity" class="form-control" placeholder="50" min="1" />
                                    <small class="error_msg" id="max_quantity_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Short Description</label>
                                    <textarea name="short_description" id="short_description" class="form-control" placeholder="Short Description"></textarea>
                                    <small class="error_msg" id="short_description_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control" placeholder="Category Description"></textarea>
                                    <small class="error_msg" id="description_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Product Info</label>
                                    <textarea name="product_info" id="product_info" class="form-control" placeholder="Product Info"></textarea>
                                    <small class="error_msg" id="product_info_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" />
                                    <small class="error_msg" id="image_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Product Images</label>
                                    <input type="file" name="product_images[]" id="product_images" class="form-control" accept="image/*" multiple />
                                    <small class="error_msg" id="product_images_msg"></small>
                                </div>
                            </div>

                            <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>
                            <input type="hidden" name="type" value="product" />
                            <div class="col-md-6 imgDiv images" style="display:none"></div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="col-md-12">
                            <button type="submit" id="btnSub" data-type="create" class="btn btn-primary mb-2" style="float:right">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>

            <div class="col-lg-12" id="details">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>



<script>

$('#formSubmit').on('submit',function(e){
    e.preventDefault()
    $('.error_msg').html('')
    var btnSub = $("#btnSub").attr('data-type')
    const formData = new FormData(this)
    var url = btnSub == 'create' ? "{{route('product.store')}}" : `{{url('/admin/product')}}/${$('#id').val()}`
    btnSub == 'update' && formData.append('_method','PUT')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        contentType: false,
        cache: false,
        processData: false,
        url: url,
        method: 'POST',
        data: formData,
        beforeSend:function(){
            $("#btnSub").prop("disabled", true);
        },
        success:function(res){
            if(res.msg == 'success'){
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                close()
                $('#btnSub').attr('data-type','create')
                getData()
            }
            else{
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                $('#btnSub').attr('data-type','create')
            }
            $("#btnSub").prop("disabled", false);
        },
        error:function(error){
            $.each(error.responseJSON.errors,function(key,value) {
                $("#"+key+"_msg").html(value);
            });
                $("#btnSub").prop("disabled", false);
        },
    })

})




$(document).on('click','.edit',function(){
    $('.error_msg').html('')
    var id = $(this).attr('data-id')
    $.ajax({
        url:`{{url('/admin/product')}}/${id}/edit`,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
                $('#name').val(res.product.name)
                $('#price').val(res.product.price)
                $('#id').val(res.product.id)
                $('#description').val(res.product.description)
                $('#short_description').val(res.product.short_description)
                $('#product_info').val(res.product.product_info)
                $(`#category option[value=${res.product.category}]`).attr('selected','selected');
                $('#img').attr('src',res.product.image)
                $('#img').attr('href',res.product.image)
                $('#quantity').val(res.product.quantity)
                $('#min_quantity').val(res.product.min_quantity)
                $('#max_quantity').val(res.product.max_quantity)
                var sub = ''
                res.sub.map((v,i) => {
                    sub += `<option value="${v.id}" ${v.id == res.product.sub_category ? 'selected' : ''}>${v.name}</option>`
                })
                $('#sub_category').html(sub)
                var html = ''
                if(res.images.length > 0){
                    res.images.map((v,i) => {
                        var url = "{{asset('uploads/images')}}/"+v.image
                        html += `<div class="col-sm-2 text-center" id="${v.id}" style="float:left; margin-left:35px">
                        <img src="${url}" href="${url}" data-fancybox="images" class="fancy-box" style="width:100px;height:100px;" /><br>
                        <a href='javascript:void(0)' class="delImage" data-id="${v.id}">Delete</a>
                        </div>`
                    })
                }
                $('.images').html(html)
                $('#btnSub').attr('data-type','update')
                $('.imgDiv').css('display','block')
                $('.title').html('Update Product')
                open()
            }
            else{
                $.notify('Unable to fetch record.', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
            }
        }
    })
})


$(document).on('click','.delImage',function(){
    var id = $(this).attr('data-id')
    $.ajax({
        url:`{{url('/admin/product/create')}}?id=${id}`,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
                $('#'+id).remove()
            }
        }
    })
})


$(document).on('click','.delete',function(){
    var id = $(this).attr('data-id')
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            url:`{{url('admin/sub-categories')}}/${id}`,
            type:"DELETE",
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    getData()
                }
                else{
                    $.notify('Error while deleting event', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
            }
        })

        }
    })

})

getData()

function getData(){
    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
          ajax:{
              url: "{{ route('product.index') }}"
           },
          columns: [
            { data: 'image', name: 'image', orderable: false },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'category', name: 'category' },
            { data: 'sub_category', name: 'sub_category' },
            { data: 'date', name: 'date' },
            { data: 'action', name: 'action', orderable: false },
        ]

    });
}

$('#category').change(function(){
    var val = $(this).val()
    $.ajax({
        url:"{{url('/admin/getSubCategory')}}/"+val,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
            if(res.data.length > 0){
                var html = ''
                res.data.map((v,i) => {
                    html += `<option value='${v.id}'>${v.name}</option>`
                })
                $('#sub_category').html(html)
            }
            else{
              $('#sub_ategory').html('')
            }
        }
        }
    })
})

$(document).on('click','.showMore',function(){
    let id = $(this).attr('data-id')
    let type = $(this).attr('data-type')
    let less = $(this).attr('data-less')
    let more = $(this).attr('data-content')
    if(type == 'less'){
        $(`#${id}`).attr('data-type','more')
        $(`#content${id}`).html(more)
        $(`#${id}`).html('[See Less]')
    }
    else if(type == 'more'){
        $(`#${id}`).attr('data-type','less')
        $(`#content${id}`).html(less)
        $(`#${id}`).html('[See More]')
    }
})

</script>
    
@endsection