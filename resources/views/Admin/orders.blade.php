@extends('layouts.admin')

@section('title') Packages @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Purchased Packages', 'type' => null ])
@endsection

@section('content')

<!--begin::Card-->
<div class="card card-custom" id="details">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label" style="text-transform:capitalize">Manage Orders</h3>
        </div>
        <div class="card-toolbar">
        <select class="form-control" id="filter">
                    <option value="0" selected hidden>Select Purchased Type</option>
                    <option value="1">All</option>
                    <option value="automotive">Automotive Package</option>
                    <option value="private">Private Seller</option>
                </select>
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover table-checkable" id="datatable" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Amount Payed</th>
                    <th>Package</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->



@endsection

@section('footer')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>-->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script src="{{ asset('/script/order.js') }}"></script>

@endsection