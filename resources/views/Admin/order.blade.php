@extends('layouts.admin')

@section('title') Orders @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => 'Product Details', 'type' => null, 'btn' => 'Create Product'  ])
@endsection

@section('content')

<div class="section-body mt-3">
    <div class="container-fluid">

        <div class="row clearfix mt-2">

            <div class="col-lg-12" id="details">
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover js-basic-example table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Country</th>
                                        <th>Total</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script> -->
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>

getData()

function getData(){
    var user = "{{ Auth::user()->user_type }}";
    var url = `{{ url('/admin/order') }}/{{ Request::segment(3) }}`;
    if(user == 'Admin'){
        url = `{{ url('/admin/order') }}/{{ Request::segment(3) }}`;
    }
    else{
        url = `{{ url('/user-order') }}/{{ Request::segment(2) }}`;
    }
    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
          ajax:{
              url: url
           },
          columns: [
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'address', name: 'address' },
            { data: 'country', name: 'country' },
            { data: 'total', name: 'total' },
            { data: 'date', name: 'date' },
            { data: 'action', name: 'action', orderable: false },
        ]

    });
}

$(document).on('change','.status',function(){
    var val = $(this).val();
    var id = $(this).attr('data-id')
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, continue!'
    }).then((result) => {
        if (result.value) {

    $.ajax({
      url:"{{url('/admin/order-status-change')}}/"+id+"/"+val,
      type:"GET",
      success:function(res){
        if(res == '1'){
          $.notify("Order Status Updated",'success');
          getData()
        }
        else{
          $.notify("Error while updating order status",'error');
        }
      }
    })

        }
    })

})

</script>
    
@endsection