@extends('layouts.admin')

@section('title') Packages @endsection

@section('header')

<link href="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />

@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => Request::segment(2).' Packages', 'type' => 'packages', 'btn' => 'Create Package'  ])
@endsection

@section('content')


<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact" id="post" style="display:none">
    <div class="card-header">
        <h3 class="card-title">Package</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" id="close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form id="formSubmit">
        <div class="card-body">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="text" class="form-control" placeholder="" name="" id="" />
                        <span class="form-text text-muted error_msg"></span>
                    </div>
                </div>

                <div class="col-md-6 imgDiv" style="display:none"><img href="" data-fancybox="images" class="fancy-box" id="img" style="width:100px; height:100px" /><input type="hidden" id="id" /></div>

                <div class="col-md-6 imgDiv images" style="display:none"></div>

            </div>

        </div>
        <div class="card-footer">
            <button type="reset" class="btn btn-primary mb-2" id="btnSub" data-type="create" style="float:right">Submit</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->



<!--begin::Card-->
<div class="card card-custom" id="details">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label" style="text-transform:capitalize">Manage {{ Request::segment(2) }} Packages</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">

            </div>
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover table-checkable" id="datatable" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <th>Record ID</th>
                    <th>Order ID</th>
                    <th>Country</th>
                    <th>Ship City</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>64616-103</td>
                    <td>Brazil</td>
                    <td>São Félix do Xingu</td>
                </tr>
                
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->



@endsection

@section('footer')

<script src="{{ asset('/dashboard/assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script>
<script src="{{ asset('/dashboard/assets/js/pages/crud/datatables/advanced/column-visibility.js?v=7.0.4') }}"></script>
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script src="{{ asset('/script/packages.js') }}"></script>

@endsection