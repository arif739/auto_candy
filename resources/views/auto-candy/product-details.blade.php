@extends('layouts.app')

@section('title') {{ $product->name }}@endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}" />
    <style type="text/css">
        .productCategoryCol .card:first-child {
           border-top: none;
        }
        .productCategoryCol {
           padding-top: 10px;
        }
        .item-slick.slick-slide.slick-current.slick-active {
        outline: none!important  
        }
        .slider-for {
        margin-bottom: 15px;
        }
        .slider-for img {
        width: 100%;
        min-height: 100%; 
        }
        .slider-nav {
        margin: auto;
        }
        .slider-nav .slick-arrow {
           display: none !important;
        }
        .slider-nav .item-slick {
        max-width: 206px;
        margin-right: 20px;
        outline: none!important;
        cursor: pointer;
        border-radius: 0px 0px 30px 0px;
        }
        .slider-nav .item-slick img {
        max-width: 100%;
        background-size: cover;
        background-position: center; 
        }
        .slick-arrow {
        position: absolute;
        top: 50%;
        z-index: 50;
        margin-top: -12px; 
        }
        .slick-prev {
        left: 0;
        }
        .slick-next {
        right: 0; 
        }
        .slider-for .slick-track img.item-slick.slick-slide.slick-current.slick-active {
        height: 468px !important;
        object-fit: cover !important;
        border-radius: 50px 0px 0px 0px;
        }
        .singleCarSlider .slick-arrow {   
        border: none;
        font-size: 0px;
        background: #ffffff;
        height: 48px;
        width: 48px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        margin-left: 20px;
        margin-right: 20px;
        cursor: pointer;
        transition:ease all 0.25s;
        }
        .singleCarSlider .slick-arrow:hover {
        background: #0018f8;
        }
        .singleCarSlider .slick-prev:before {
        position: absolute;
        content: '\f053';
        font-family: 'Font Awesome 5 Free';
        font-size: 14px;
        color: #d0d0d0;    
        font-weight: 700;
        }
        .singleCarSlider .slick-next:before {
        position: absolute;
        content: '\f054';
        font-family: 'Font Awesome 5 Free';
        font-size: 14px;
        color: #d0d0d0;    
        font-weight: 700;
        }
        .mainBody.singleVehicleBody {
           padding-bottom: 0px;
        }
        /*.quantity-counter {
               display: flex;
               flex-direction: column;
               width: 40px;
           }*/
            .quantity-counter .value {
               border: 1px solid #adadad;
               border-radius: 3px;
               margin: 2px 0;
               padding: 4px;
               text-align: center;
               -moz-appearance: textfield;
               width: 40px;
           }
            .quantity-counter .value::-webkit-outer-spin-button, .quantity-counter .value::-webkit-inner-spin-button {
               -webkit-appearance: none;
               margin: 0;
           }
            .quantity-counter .increment, .quantity-counter .decrement {
               border: 0;
               background: transparent;
               cursor: pointer;
               border-radius: 3px; 
               background: #0018f8;
               color: #fff;
               height: 40px;
              width: 40px;
           }
     </style>
@endsection

@section('banner')
<section class="dynamicHeaderInner hero  newVehicleSec">
    <div class="container">
       <div class="heading">
          <h3>
             {{ $product->name }}
          </h3>
       </div>
    </div>
 </section>
@endsection

@section('content')
    

<div class="mainBody singleVehicleBody">
    <section class="blogs-news listing-featured-vehicles featured-vehicles pt-0 pb-0">
       <div class="container">
          <div class="row">                     
             <div class="col-md-3 productCategoryCol productListingSidebar">                    
                   <div class="accordion" id="accordionExample">
                    @foreach($categories as $key=>$cat)
                    <div class="card z-depth-0 bordered changeIcon">
                       <div class="card-header" id="{{ $key }}">
                          <h5 class="mb-0">
                              @php
                                $collapse = null;
                                $selected = null;
                                  if(request()->get('sub')){
                                    $catCheck = App\Models\SubCategory::where('name',request()->get('sub'))->first();
                                    $collapse = $catCheck->category == $cat->id ? 'show' : '';
                                    $selected = $catCheck->id;
                                  }
                                @endphp
                             <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo{{$key}}"
                                aria-expanded="true" aria-controls="collapseTwo{{$key}}">
                                {{ $cat->name }}
                             </button>
                          </h5>
                       </div>
                       <div id="collapseTwo{{$key}}" class="collapse  {{ $collapse ? $collapse : '' }}" aria-labelledby="{{ $key }}"
                          data-parent="#accordionExample">
                          <div class="card-body">
                             <ul>
                                  @foreach($subCategories as $sub)
                                      @if($cat->id == $sub->category)
                                          <li><a href="{{ url('/products') }}?sub={{ $sub->name }}" class="{{ $selected == $sub->id ? 'selectedTab' : '' }}">{{ $sub->name }}</a></li>
                                      @endif
                                  @endforeach
                             </ul>
                          </div>
                       </div>
                    </div>
                    @endforeach                        
                   </div>
                </div>  
             <div class="col-md-9 singleCarSlider">
                <div class="container pl-0 pr-0">
                   <div class="slider-container">
                      <div class="slider-for">
                         <img class="item-slick" src="{{ asset('/uploads/product/'.$product->image) }}" alt="Alt">
                         @foreach($images as $key=>$img)
                         <img class="item-slick" src="{{ asset('/uploads/images/'.$img->image) }}" alt="Alt">
                         @endforeach 
                      </div>
                      <div class="slider-nav"> 
                        <img class="item-slick" src="{{ asset('/uploads/product/'.$product->image) }}" alt="Alt">
                        @foreach($images as $key=>$img)
                        <img class="item-slick" src="{{ asset('/uploads/images/'.$img->image) }}" alt="Alt">
                        @endforeach 
                      </div>
                   </div>
                </div>
                <div class="row">
                   <div class="col-md-12 item">
                      <h4 class="dynamicHeading mt-3 mb-3">{{ $product->name }}</h4>
                      <div class="category-box">
                         {{--  <h4>
                            Automative Dealer
                         </h4>  --}}
                      </div>
                   </div>
                   <div class="col-md-12 mt-3 mb-3 singleProductData">
                      Price: <span>$</span><input type="text" class="borderlessInput text-left" name="ProductPrice" value="{{ number_format($product->price) }}">
                   </div>
                   <div class="quantity-counter col-md-3">
                     <button id="counter-decrement" class="decrement">-</button>
                     <input id="counter-value" class="value" type="number" value="{{ $product->min_quantity }}" min="{{ $product->min_quantity }}" max="{{ $product->max_quantity }}">
                     <button id="counter-increment" class="increment">+</button>
                   </div>
                   <div class="col-md-9  mt-2 mb-3">
                      <a href="javascript:void(0)" class="blueBtn pl-4 add-cart" data-id="{{ $product->id }}">ADD TO CART</a>
                   </div>
                   <div class="col-md-12 productDescription">
                      <h4 class="dynamicHeading mt-3 mb-3">Product Description</h4>
                      <p>{{ $product->description }}</p>
                   </div>

                   <div class="col-md-12 productDescription">
                        <h4 class="dynamicHeading mt-3 mb-3">Product Information</h4>
                        <p>{{ $product->product_info }}</p>
                    </div>
                </div>
             </div>
          </div>
       </div>
    </section>
    @if(count($similar) > 0)
    <section class="featured-vehicles">
       <div class="container">
           <div class="row">
               <div class="col-md-3">                           
                   <div class="addvertisment-box">
                       <img src="./assets/images/add/60months.jpg" alt="">
                   </div>
               </div>
               <div class="col-md-9">
                   <div class="head-desc">
                       <h3>
                           YOU
                       </h3>
                       <h2>
                           MAY ALSO LIKE
                       </h2>
                   </div>
                   <div class="featured-vehicle-slider home-feature-vehicle-slider">
                        @foreach($similar as $sim)
                       <div class="item">
                           <div class="img-box">
                               <img src="{{ asset('/uploads/product/'.$sim->image) }}" alt="">
                           </div>
                           <div class="desc-box">
                               <div class="title-box">
                                   <h3>
                                       {{ $sim->name }}
                                   </h3>
                               </div>
                               <div class="price-box">
                                   <h4>
                                       $ {{ number_format($sim->price) }}
                                   </h4>
                               </div>
                           </div>
                       </div>
                       @endforeach
                   </div>
               </div>
           </div>
       </div>
   </section>
   @endif
 </div>


@endsection

@section('footer')
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script type="text/javascript">   
    $(function() { 
    // Card's slider
    var $carousel = $('.slider-for');
    
      $carousel
        .slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          adaptiveHeight: true,
          infinite: true,
          asNavFor: '.slider-nav'
        })
        
      $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: true,
        variableWidth: true
      });
    });
 </script>
 <script type="text/javascript">
    var counterValue = document.querySelector("#counter-value");
    var counterIncrement = document.querySelector("#counter-increment");
    var counterDecrement = document.querySelector("#counter-decrement");
    var count = parseInt("{{ $product->min_quantity }}");
    var min = parseInt("{{ $product->min_quantity }}")
    var max = parseInt("{{ $product->max_quantity }}")

    this.counterIncrement.addEventListener('click', () => {
        if(count >= max){
            $.notify(`Maximum product order quantity is ${max}`, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        }
        else{
            this.count++
            this.counterValue.setAttribute("value", count);
        }
    });

    this.counterDecrement.addEventListener('click', () => {
        if(count <= min){
            $.notify(`Minimum product order quantity is ${min}`, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        }
        else{
            this.count--
            this.counterValue.setAttribute("value", count);
        }
        //if(count > 1){
          //  this.count--
            //this.counterValue.setAttribute("value", count);
        //}
    });
 </script>
@endsection