@extends('layouts.app')

@section('title') Home @endsection

@section('header')

<link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}">

@endsection

@section('banner')
<section class="dynamicHeaderInner hero  packagesSec">
            <div class="container">
                <div class="heading">
                    <h3>
                        PACKAGES
                    </h3>
                </div>                
            </div>
        </section>
@endsection

@section('content')

<div class="mainBody">
            <section class="blogs-news pt-0 pb-0">
                <div class="container">
                    <div class="row">                        
                        <div class="col-md-12 pl-0">
                            <div class="head-desc">
                                <h3>
                                    Automotive Dealer Packages Available
                                </h3>
                                <h2>
                                    Dealer Pricing
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="packagesDetails packagesDetailsAutomotive">
                <div class="container">
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert') }}" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                    <div class="row packagesDetailsRow">

                        @foreach($packages as $pkg)
                        <div class="col-md-4 packageCol">
                            <h4><span>{{ $pkg->title }}</span></h4>
                            <h3>{{ $pkg->package_type == 'Paid' ? '$'.$pkg->price : $pkg->trial_days.' Days' }}</h3>
                            <ul class="packageColList mb-4">
                                <li>{{ $pkg->duration == 1 ? 'One Time Only' : '30 Days' }}</li>
                                <li>{{ $pkg->vehicle }} accessorized vehicles</li>
                                <li>{{ $pkg->image }} pictures per vehicle provided by the Automotive Dealer</li>
                                <li>{{ $pkg->video }} video link provided by the Automotive Dealer for each vehicle listing.</li>
                                <li>Unused listings do not roll-over. No refunds or credits will be issued for unused or deleted listings.</li>
                                <li>Vehicles are non-factory installed accessorized vehicles.</li>
                            </ul>
                            <div class="row">
                                <div class="col-md-12 text-center mt-4">
                                @if(Auth::check())
                                        @if(auth()->user()->hasPackage())
                                            @if(auth()->user()->packageDetails()->package_id == $pkg->id)
                                            <a href="javascript::void(0)" class="getBtn" disabled>Purchased</a>
                                            @else
                                            <a href="{{ url('/checkout/'.Crypt::encrypt($pkg->id)) }}" class="getBtn">Buy Now</a>
                                            @endif
                                        @else
                                            <a href="{{ url('/checkout/'.Crypt::encrypt($pkg->id)) }}" class="getBtn">Buy Now</a>    
                                        @endif
                                    @else
                                        <a href="{{ url('/checkout/'.Crypt::encrypt($pkg->id)) }}" class="getBtn">Buy Now</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <!-- <div class="row">
                        <div class="col-md-4 packageCol">
                            <h4><span>Premium Package</span></h4>
                            <h3>$800.00</h3>
                            <ul class="packageColList mb-4">
                                <li>Once Per Month</li>
                                <li>40 accessorized vehicles</li>
                                <li>20 pictures per vehicle provided by the Automotive dealer</li>
                                <li>1 video link provided by the Automotive Dealer for each vehicle listing.</li>
                                <li>Unused listings do not roll-over. No refunds or credits will be issued for unused or deleted listings.</li>
                                <li>Vehicles are non-factory installed accessorized vehicles.</li>
                            </ul>
                            <div class="row">
                                <div class="col-md-12 text-center mt-4">                                    
                                    <a href="#" class="getBtn">Buy Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 packageCol">
                            <h4><span>Ultra Package</span></h4>
                            <h3>$1500.00</h3>
                            <ul class="packageColList mb-4">
                                <li>Once Per Month</li>
                                <li>100 accessorized vehicles</li>
                                <li>20 pictures per vehicle provided by the Automotive dealer</li>
                                <li>1 video link provided by the Automotive Dealer for each vehicle listing.</li>
                                <li>Unused listings do not roll-over. No refunds or credits will be issued for unused or deleted listings.</li>
                                <li>Vehicles are non-factory installed accessorized vehicles.</li>
                            </ul>
                            <div class="row">
                                <div class="col-md-12 text-center mt-4">
                                    <a href="#" class="getBtn">Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </section>
        </div> 



@endsection

@section('footer')
    
@endsection