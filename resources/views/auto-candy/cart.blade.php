@extends('layouts.app')

@section('title') Cart @endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}" />
@endsection

@section('banner')
<section class="dynamicHeaderInner hero  packagesSec">
    <div class="container">
       <div class="heading">
          <h3>
             CART
          </h3>
       </div>
    </div>
 </section>
@endsection

@section('content')
    

<section class="cartForm">
    <div class="container">
        @if(Session::has('cart'))
       <table id="cart" class="table table-hover table-condensed">
          <thead>
             <tr>
                <th style="width:50%">Product</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Quantity</th>
                <th style="width:22%" class="text-center">Subtotal</th>
                <th style="width:10%">Action</th>
             </tr>
          </thead>
          <tbody>
            @php $total = 0.0; @endphp
            @foreach($cart as $c)
            <tr class="borderRow" id="{{ $c['id'] }}">
                <td data-th="Product">
                  <div class="row">
                    <div class="col-sm-2 hidden-xs "><img style="width: 100%;" src="{{ $c['img'] }}" alt="..." class="img-responsive"></div>
                    <div class="col-sm-10">
                      <h4 class="nomargin">{{ $c['name'] }}</h4>
                      <p>{{ strlen($c['description']) > 80 ? substr($c['description'],0,80) : $c['description'] }}</p>
                    </div>
                  </div>
                </td>
                <td data-th="Price">${{ number_format($c['price']) }}</td>
                <td data-th="Quantity">
                  <input type="number" class="form-control text-center quantity" data-id="{{ $c['id'] }}" value="{{ $c['quantity'] }}">
                </td>
                <td id="subtotal{{ $c['id'] }}"   class="text-center">${{ number_format($c['total']) }}</td>
                <td class="actions">
                  <button class="btn btn-danger btn-sm delete" data-id="{{ $c['id'] }}"><i class="fa fa-trash"></i></button>                
                </td>
              </tr>
             @php $total += (float)$c['total'] @endphp
            @endforeach
          </tbody>
          <tfoot>
             <tr>
                <td style="padding-left: 0px;"><a href="{{ url('/products') }}" class="btn  getBtn"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
              <td colspan="2" class="hidden-xs"></td>
              <td class="hidden-xs text-center"><strong>Total $<span id="totalAmount">{{ number_format($total) }}</span></strong></td>
              <td style="padding-right: 0px;"><a href="{{ url('/product-checkout') }}" class="btn  btn-block getBtn">Checkout <i class="fa fa-angle-right"></i></a></td>
             </tr>
          </tfoot>
       </table>
       @endif
    </div>
 </section>


@endsection

@section('footer')
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>
$('.quantity').change(function(){
    let id = $(this).attr('data-id')
    let quant = $(this)
    let quantity = $(this).val()
    var formData = new FormData();
    formData.append('id',id)
    formData.append('quantity',quantity)
    formData.append('_method','PUT')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        url:`{{ url('/cart') }}/${id}`,
        type:'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success:function(res){
            if(res.msg == 'success'){
                $(`#subtotal${id}`).html(`$${res.sub}`)
                $('#totalAmount').html(res.total)
            }
            else{
              $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
              quant.val(res.val)
            }
        }
    });
})

$('.delete').click(function(e){
    e.preventDefault()
    let id = $(this).attr('data-id')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        url:`{{ url('/cart') }}/${id}`,
        type:'DELETE',
        success:function(res){
            if(res.msg == 'success'){
                $(`#${id}`).remove()
                $('#totalAmount').html(res.total)
                getCartCount()
            }
        }
    });
})
</script>   
@endsection