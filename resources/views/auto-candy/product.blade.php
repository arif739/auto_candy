@extends('layouts.app')

@section('title') Products @endsection

@section('header')
<link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}">   
@endsection

@section('banner')
<section class="dynamicHeaderInner hero  newVehicleSec">
    <div class="container">
        <div class="heading">
            <h3>
                Product Listing
            </h3>
        </div>                
    </div>
</section> 
@endsection

@section('content')
<div class="mainBody">
    <section class="blogs-news listing-featured-vehicles featured-vehicles pt-0 pb-0">
        <div class="container">
            <div class="row"> 
                <div class="col-md-12">
                    <div class="head-desc">
                        <h3>
                            FEATURED
                        </h3>
                        <h2>
                            PRODUCTS
                        </h2>
                    </div>
                </div>
                <div class="col-md-3 productCategoryCol productListingSidebar">                     
               <div class="accordion" id="accordionExample">

                @foreach($categories as $key=>$cat)
                  <div class="card z-depth-0 bordered changeIcon">
                     <div class="card-header" id="{{ $key }}">
                        <h5 class="mb-0">
                            @php
                              $collapse = null;
                              $selected = null;
                                if(request()->get('sub')){
                                  $catCheck = App\Models\SubCategory::where('name',request()->get('sub'))->first();
                                  $collapse = $catCheck->category == $cat->id ? 'show' : '';
                                  $selected = $catCheck->id;
                                }
                              @endphp
                           <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo{{$key}}"
                              aria-expanded="true" aria-controls="collapseTwo{{$key}}">
                              {{ $cat->name }}
                           </button>
                        </h5>
                     </div>
                     <div id="collapseTwo{{$key}}" class="collapse  {{ $collapse ? $collapse : '' }}" aria-labelledby="{{ $key }}"
                        data-parent="#accordionExample">
                        <div class="card-body">
                           <ul>
                                @foreach($subCategories as $sub)
                                    @if($cat->id == $sub->category)
                                        <li><a href="{{ url('/products') }}?sub={{ $sub->name }}" class="{{ $selected == $sub->id ? 'selectedTab' : '' }}">{{ $sub->name }}</a></li>
                                    @endif
                                @endforeach
                           </ul>
                        </div>
                     </div>
                  </div>
                  @endforeach

               </div>
            </div>                       
                <div class="col-md-9 featured-vehicles">
                    {{--  <div class="row pl-3 pr-3">                                
                        <div class="col-md-12 gridCol">
                            <span>Showing 1 to 9 of 134 </span>
                            <div class="rightController">
                                <select>
                                    <option>Sort by: Default</option>
                                    <option>Sort by: Default</option>
                                    <option>Sort by: Default</option>
                                </select>
                                <i class="fas fa-chevron-down"></i>          
                            <i class="fas fa-th"></i>
                            </div>                
                        </div>  
                    </div>                               --}}
                    <div class="row featured-vehicle-slider productArchive">

                        @foreach($products as $product)
                        <div class="item col-md-6 mr-0 ml-0">
                            <div class="img-box">
                                <img src="{{ asset('/uploads/product/'.$product->image) }}" alt="">
                            </div>
                            <div class="desc-box">
                                <div class="title-box">
                                    <h3>
                                        {{ $product->name }}
                                    </h3>
                                </div>
                                <div class="price-box">
                                    <h4>
                                        ${{ number_format($product->price) }}
                                    </h4>
                                </div>
                                
                              <div class="btnDetail">  
                                <div class="cartBox">
                                  <a href="javascript:void(0)" class="blueBtn pl-4 add-cart" data-id="{{ $product->id }}">ADD TO CART</a>
                                </div>
                                <div class="viewDetail actionBtn p-0">
                                  <a href="{{ url('/product/'.Crypt::encrypt($product->id)) }}" class="blueBorderBtn"> View Details</a>
                                </div>
                              </div>  
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                    <div class="row pl-3 pr-3">  
                        
                        <div class="col-md-12 text-center loadMore">
                            {!! $products->links("pagination::bootstrap-4") !!}
                        </div>
                        

                        {{--  <div class="col-md-12 gridCol">
                            <span>Showing 1 to 9 of 134 </span>
                            <span class="paginationNo">3</span>
                            <span class="paginationNo">2</span>
                            <span class="paginationNo">1</span>
                            <!-- <div class="rightController">
                                <select>
                                    <option>Sort by: Default</option>
                                    <option>Sort by: Default</option>
                                    <option>Sort by: Default</option>
                                </select>
                                <i class="fas fa-chevron-down"></i>          
                                <i class="fas fa-th"></i>
                            </div> -->                
                        </div>    --}}
                    </div>
                </div>                        
            </div>
        </div>
    </section>
</div> 
@endsection

@section('footer')
    <script src="{{ asset('/assets/js/custom.js') }}"></script>
@endsection