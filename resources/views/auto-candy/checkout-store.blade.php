@extends('layouts.app')

@section('title') Product Checkout @endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}" />
@endsection

@section('banner')
<section class="dynamicHeaderInner hero  packagesSec">
    <div class="container">
        <div class="heading">
            <h3>
                CHECKOUT
            </h3>
        </div>                
    </div>
</section>    
@endsection

@section('content')
 

<section class="checkoutForm blogs-news">
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
              <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-dark">Your cart</span>
                <span class="badge badge-secondary badge-pill bg-dark count">{{ $count }}</span>
              </h4>
              <ul class="list-group mb-3">
              @php $total = 0.0; @endphp
                @foreach($cartData as $cart)
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                  <div>
                    <h6 class="my-0">{{ $cart['name'] }}</h6>
                    <small class="text-muted">{{ substr($cart['description'],0,50) }}</small>
                  </div>
                  <span class="text-muted">${{ number_format($cart['total']) }}</span>
                </li>
                @php $total += (float)$cart['total'] @endphp
                @endforeach

                
                <li class="list-group-item d-flex justify-content-between" id="promo" style="display:none !important">
                  <div>
                    <h6 class="my-0">Promo code</h6>
                    <small id="couponCodeTitle"></small>
                  </div>
                  <span id="couponValue">$5</span>
                </li>

                <li class="list-group-item d-flex justify-content-between">
                  <span>Total (USD)</span>
                  <strong>$<span id="total">{{ number_format($total) }}</span></strong>
                </li>
              </ul>

              <form class="card p-2">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Promo code" id="code">
                  <div class="input-group-append">
                    <button type="button" class="btn  getBtn ml-0" id="codeBtn">Redeem</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-8 order-md-1">
              <h4 class="mb-3">Billing address</h4>
              <form class="needs-validation" novalidate="" id="checkoutForm">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="firstName">name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="" value="" required="">
                    <small class="error_msg" id="name_msg"></small>
                  </div>
                  <div class="col-md-6 mb-3">
                    <label for="lastName">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="" value="" required="">
                    <small class="error_msg" id="email_msg"></small>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="">
                  <div class="invalid-feedback">
                    Please enter your shipping address.
                  </div>
                  <small class="error_msg" id="address_msg"></small>
                </div>

                <div class="mb-3">
                  <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                  <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite">
                </div>

                <div class="row">
                  <div class="col-md-5 mb-3">
                    <label for="country">Country</label>
                    <select class="custom-select d-block w-100" id="country" name="country" required="">
                      <option value="0" selected hidden>Choose...</option>
                      <option>United States</option>
                    </select>
                    <small class="error_msg" id="country_msg"></small>
                  </div>
                  <div class="col-md-4 mb-3">
                    <label for="state">State</label>
                    <select class="custom-select d-block w-100" id="state" name="state" required="">
                      <option value="0" selected hidden>Choose...</option>
                      @foreach($states as $state)
                      <option value="{{ $state }}">{{ $state }}</option>
                      @endforeach
                    </select>
                    <small class="error_msg" id="state_msg"></small>
                  </div>
                  <div class="col-md-3 mb-3">
                    <label for="zip">Zip</label>
                    <input type="number" class="form-control" id="zip" name="zip" placeholder="" required="">
                    <small class="error_msg" id="zip_msg"></small>
                  </div>
                </div>
                <hr class="mb-4">                
                <!-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="same-address">
                  <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
                </div> -->
                <!-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="save-info">
                  <label class="custom-control-label" for="save-info">Save this information for next time</label>
                </div> -->
                <!-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" data-toggle="collapse" data-target="#shippingAddress" class="custom-control-input" id="shipping-address">
                  <label class="custom-control-label" for="shipping-address">Change Shipping Address</label>
                  <div id="shippingAddress" class="collapse">
                    <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">First name</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
                        <div class="invalid-feedback">
                          Valid first name is required.
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Last name</label>
                        <input type="text" class="form-control" id="lastName" placeholder="" value="" required="">
                        <div class="invalid-feedback">
                          Valid last name is required.
                        </div>
                      </div>
                    </div>

                    <div class="mb-3">
                      <label for="username">Username</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">@</span>
                        </div>
                        <input type="text" class="form-control" id="username" placeholder="Username" required="">
                        <div class="invalid-feedback" style="width: 100%;">
                          Your username is required.
                        </div>
                      </div>
                    </div>

                    <div class="mb-3">
                      <label for="email">Email <span class="text-muted">(Optional)</span></label>
                      <input type="email" class="form-control" id="email" placeholder="you@example.com">
                      <div class="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                      </div>
                    </div>

                    <div class="mb-3">
                      <label for="address">Address</label>
                      <input type="text" class="form-control" id="address" placeholder="1234 Main St" required="">
                      <div class="invalid-feedback">
                        Please enter your shipping address.
                      </div>
                    </div>

                    <div class="mb-3">
                      <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                      <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
                    </div>

                    <div class="row">
                      <div class="col-md-5 mb-3">
                        <label for="country">Country</label>
                        <select class="custom-select d-block w-100" id="country" required="">
                          <option value="">Choose...</option>
                          <option>United States</option>
                        </select>
                        <div class="invalid-feedback">
                          Please select a valid country.
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="state">State</label>
                        <select class="custom-select d-block w-100" id="state" required="">
                          <option value="">Choose...</option>
                          <option>California</option>
                        </select>
                        <div class="invalid-feedback">
                          Please provide a valid state.
                        </div>
                      </div>
                      <div class="col-md-3 mb-3">
                        <label for="zip">Zip</label>
                        <input type="text" class="form-control" id="zip" placeholder="" required="">
                        <div class="invalid-feedback">
                          Zip code required.
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <hr class="mb-4">

                <h4 class="mb-3">Payment</h4>

                <!-- <div class="d-block my-3">
                  <div class="custom-control custom-radio">
                    <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked="" required="">
                    <label class="custom-control-label" for="credit">Credit card</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required="">
                    <label class="custom-control-label" for="debit">Debit card</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required="">
                    <label class="custom-control-label" for="paypal">PayPal</label>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="cc-name">Name on card</label>
                    <input type="text" class="form-control" id="card_name" name="card_name" placeholder="" required="">
                    <small class="text-muted">Full name as displayed on card</small>
                    <div class="invalid-feedback">
                      Name on card is required
                    </div>
                    <small class="error_msg" id="card_name_msg"></small>
                  </div>
                  <div class="col-md-6 mb-3">
                    <label for="cc-number">Credit card number</label>
                    <input type="number" class="form-control" id="card_number" name="card_number" placeholder="" required="">
                    <div class="invalid-feedback">
                      Credit card number is required
                    </div>
                    <small class="error_msg" id="card_number_msg"></small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Expiration Month</label>
                    <input type="number" class="form-control" id="expiration_month" name="expiration_month" placeholder="12" required="">
                    <div class="invalid-feedback">
                      Expiration month required
                    </div>
                    <small class="error_msg" id="expiration_month_msg"></small>
                  </div>
                  <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Expiration Year</label>
                    <input type="number" class="form-control" id="expiration_year" name="expiration_year" placeholder="2024" required="">
                    <div class="invalid-feedback">
                      Expiration year required
                    </div>
                    <small class="error_msg" id="expiration_year_msg"></small>
                  </div>
                  <div class="col-md-3 mb-3">
                    <label for="cc-cvv">CVV</label>
                    <input type="number" class="form-control" id="cvv" name="cvv" placeholder="123" required="">
                    <div class="invalid-feedback">
                      Security code required
                    </div>
                    <small class="error_msg" id="cvv_msg"></small>
                  </div>
                </div>
                <hr class="mb-4">
                <button class="btn btn-lg btn-block getBtn ml-0" type="submit" id="btnSub">Checkout</button>
              </form>
            </div>
        </div>
    </div>
</section>


@endsection

@section('footer')
   

<script src="{{ asset('/stripe/stripe.js') }}"></script>
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>

$('#codeBtn').click(function(e){
  e.preventDefault()
  let val = $('#code').val()
  if(val){
    $.ajax({
      url:`{{ url('/coupon-redeem') }}/${val}`,
      type:'GET',
      success:function(res){
        if(res.msg == 'success'){
          $('#couponCodeTitle').html(res.title)
          $('#couponValue').html(res.dis)
          $('#total').html(parseFloat(res.total).toFixed(2))
          $('#promo').show(500)
          $.notify('Coupon Applied', { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
        }
        else{
          $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        }
      }
    });
  }
  else{
    $.notify('Promo Code Field Is Required', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
  }
})


$('#checkoutForm').on('submit',function(e){
  e.preventDefault()
    $('.error_msg').html('')
    if(validate()){
      Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
      Stripe.createToken({
        number: $('#card_number').val(),
        cvc: $('#cvv').val(),
        exp_month: $('#expiration_month').val(),
        exp_year: $('#expiration_year').val()
      }, stripeResponseHandler);
    }
})


function stripeResponseHandler(status, response) {
        if (response.error) {
                $.notify(response.error.message, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        } else {
            var token = response['id'];
            var name = $('#name').val()
            var email = $('#email').val()
            var address = $('#address').val()
            var address2 = $('#address2').val()
            var country = $('#country').val()
            var state = $('#state').val()
            var zip = $('#zip').val()
            var formData = new FormData()
            formData.append('name',name)
            formData.append('email',email)
            formData.append('address',address)
            formData.append('address2',address2)
            formData.append('country',country)
            formData.append('state',state)
            formData.append('zip',zip)
            formData.append('stripeToken',token)
            $.ajaxSetup({
                headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            });
            $.ajax({
                url: "{{ url('/product-checkout') }}",
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function(){
                    $("#btnSub").prop("disabled", true);
                },
                success:function(res){
                    if(res.msg == 'success'){
                      $.notify(res.res, { globalPosition:"top center", autoHideDelay: 7000, className:'success' });
                      setTimeout(() => {
                        window.location.href = "{{ url('/home')}}"
                      },3000)
                    }
                    else{
                      $.notify('Error while placing order', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                    }
                    $("#btnSub").prop("disabled", false);
                },
                error:function(error){
                    $.each(error.responseJSON.errors,function(key,value) {
                    $("#"+key+"_msg").html(value);
                });
                $("#btnSub").prop("disabled", false);
                },
            });

        }
    }




function validate(){
  var cardName = $('#card_name').val()
  var cardNumber = $('#card_number').val()
  var expireMonth = $('#expiration_month').val()
  var expireYear = $('#expiration_year').val()
  var cvv = $('#cvv').val()
  if(cardName == ''){
    $('#cardName_msg').html('Card name is required')
    return false
  }
  else if(cardNumber == ''){
    $('#cardNumber_msg').html('Card number is required')
    return false
  }
  else if(cardNumber.trim().length !== 16 ){
    $('#cardNumber_msg').html('Invalid Card Number format')
    return false
  }
  else if(expireMonth == ''){
    $('#expireMonth_msg').html('Card expiration month is required')
    return false
  }
  else if(expireYear == ''){
    $('#expireYear_msg').html('Card expiration is required')
    return false
  }
  else if(cvv == ''){
    $('#cvv_msg').html('card cvv number is required')
    return false
  }
  else if(cvv.trim().length < 3){
    $('#cvv_msg').html('Invalid cvv number format')
    return false
  }
  else{
    return true
  }
}


</script>


@endsection