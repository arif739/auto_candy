@extends('layouts.app')

@section('title') Home @endsection

@section('header')
    
@endsection

@section('banner')
    @include('layouts.home-banner')
@endsection

@section('content')
    


<section class="what-drive-us">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="addvertisment-box">
                    <img src="{{ asset('/assets/images/add/60months.jpg') }}" alt="">
                </div>
                <div class="addvertisment-box">
                    <img src="{{ asset('/assets/images/add/56months.jpg') }}" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="desc">
                    <h3>
                        What Drives Us
                    </h3>
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression of how actual text will appear in its final presentation. This
                        is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate
                        the look and feel of finished, typeset text.
                    </p>
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed.
                    </p>
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression of how actual text will appear in its final presentation. This
                        is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate
                        the look and feel of finished, typeset text.This is dummy copy. It is not meant to be
                        read.
                    </p>
                    <div class="cta-box">
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="truck-img-box">
                    <img src="{{ asset('/assets/images/what-drives-us-img.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sale-purchase">
    <div class="container">
        <div class="divider">
            <div class="buy-box">
                <div class="desc">
                    <h4>
                        Are you Looking To Buy An
                    </h4>
                    <h3>
                        accessorized vehicle?
                    </h3>
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate.
                    </p>
                    <div class="cta-box">
                        <a href="#">
                            Click Here
                        </a>
                    </div>
                </div>
            </div>
            <div class="sell-box">
                <div class="desc">
                    <h4>
                        Are you Looking To Sell An
                    </h4>
                    <h3>
                        accessorized vehicle?
                    </h3>
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate.
                    </p>
                    <div class="cta-box">
                        <a href="#">
                            Click Here
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="featured-vehicles">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="addvertisment-box">
                    <img src="{{ asset('/assets/images/add/60months.jpg') }}" alt="">
                </div>
            </div>
            <div class="col-md-9">
                <div class="head-desc">
                    <h3>
                        Featured
                    </h3>
                    <h2>
                        Accessorized Vehicles
                    </h2>
                </div>
                <div class="featured-vehicle-slider home-feature-vehicle-slider">
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-1.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 1
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Automative Dealer
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-2.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 2
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Private Seller
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-3.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 3
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Automative Dealer
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-1.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 1
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Automative Dealer
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-2.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 2
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Private Seller
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img src="{{ asset('/assets/images/vehicle-3.jpg') }}" alt="">
                        </div>
                        <div class="desc-box">
                            <div class="title-box">
                                <h3>
                                    Dummy Title Of Accessorized Vehicles 3
                                </h3>
                            </div>
                            <div class="price-box">
                                <h4>
                                    $ 20,000
                                </h4>
                            </div>
                            <div class="rating-box">
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star checked"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="category-box">
                                <h4>
                                    Automative Dealer
                                </h4>
                            </div>
                            <ul>
                                <li class="mileage-box">
                                    <p>
                                        80,000 Mileage
                                    </p>
                                </li>
                                <li class="consumtion-box">
                                    <p>
                                        <i class="fa fa-gas-pump"></i> 67 City/56 Highway
                                    </p>
                                </li>
                                <li class="location-box">
                                    <p>
                                        <i class="fa fa-map-marker-alt"></i> Dallas, TX
                                    </p>
                                </li>
                                <li class="transmition-box">
                                    <p>
                                        <i class="transimition"></i> Automatic
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blogs-news">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="addvertisment-box">
                    <img src="{{ asset('/assets/images/add/60months.jpg') }}" alt="">
                </div>
                <div class="addvertisment-box">
                    <img src="{{ asset('/assets/images/add/56months.jpg') }}" alt="">
                </div>
            </div>
            <div class="col-md-9">
                <div class="head-desc">
                    <h3>
                        Latest
                    </h3>
                    <h2>
                        News And Blogs
                    </h2>
                </div>
                <div class="blogs-tabs">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="latest-post-tab" data-toggle="tab"
                                href="#latest-post" role="tab" aria-controls="latest-post"
                                aria-selected="true">Latest Post</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="events-tab" data-toggle="tab" href="#events" role="tab"
                                aria-controls="events" aria-selected="false">Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" role="tab"
                                aria-controls="videos" aria-selected="false">Videos</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="latest-post" role="tabpanel"
                            aria-labelledby="latest-post-tab">
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest News 1
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest News 2
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest News 3
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest News 4
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="load-more-cta">
                                <a href="#" id="latest-post-loadmore">
                                    Load More
                                </a>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="events" role="tabpanel" aria-labelledby="events-tab">
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Events 1
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Events 2
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Events 3
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Events 4
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="load-more-cta">
                                <a href="#" id="latest-events-loadmore">
                                    Load More
                                </a>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab">
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Videos 1
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Videos 2
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Max Smith
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Videos 3
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="img-box">
                                            <img src="{{ asset('/assets/images/news-img-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="desc">
                                            <div class="post-meta">
                                                <div class="poster-name">
                                                    <h3>
                                                        Merry Lana
                                                    </h3>
                                                </div>
                                                <div class="posted-date">
                                                    <h4>
                                                        15-10-2020
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="heading">
                                                <h3>
                                                    Dummy Title of Latest Videos 4
                                                </h3>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    This is dummy copy. It is not meant to be read. It has been
                                                    placed here solely to demonstrate the look and feel of
                                                    finished,
                                                </p>
                                            </div>
                                            <div class="cta-box">
                                                <a href="#">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="load-more-cta">
                                <a href="#" id="latest-videos-loadmore">
                                    Load More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonials">
    <div class="container">
        <div class="head-desc">
            <h3>
                Our
            </h3>
            <h2>
                Testimonials
            </h2>
        </div>
        <div class="testimonials-slider testimonial-slider-trigger">
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Max Smith
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Albert King
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Liza Raw
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Max Smith
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Albert King
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="content">
                    <p>
                        This is dummy copy. It is not meant to be read. It has been placed here solely to
                        demonstrate the look and feel of finished, typeset text. Only for show. He who searches
                        for meaning here will be sorely disappointed. These words are here to provide the reader
                        with a basic impression.
                    </p>
                </div>
                <div class="name-rating-box">
                    <div class="name">
                        <h3>
                            Liza Raw
                        </h3>
                    </div>
                    <div class="rating">
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="logo-sect">
    <div class="container">
        <div class="logo-slider">
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-1.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Acura
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-2.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        BMW
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-3.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Chevrolet
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-4.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Ford
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-5.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Honda
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-6.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Hyundai
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-7.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Mazda
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-1.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Acura
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-2.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        BMW
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-3.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Chevrolet
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-4.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Ford
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-5.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Honda
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-6.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Hyundai
                    </h3>
                </div>
            </div>
            <div class="item">
                <div class="img-box">
                    <img src="{{ asset('/assets/images/logo-slide-7.png') }}" alt="">
                </div>
                <div class="desc">
                    <h3>
                        Mazda
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection

@section('footer')
    
@endsection