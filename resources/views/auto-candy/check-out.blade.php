@extends('layouts.app')

@section('title') Home @endsection

@section('header')

<link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}">

@endsection

@section('banner')
<section class="dynamicHeaderInner hero  packagesSec">
            <div class="container">
                <div class="heading">
                    <h3>
                        CHECKOUT
                    </h3>
                </div>                
            </div>
        </section>
@endsection

@section('content')

<section class="checkoutForm blogs-news">
            <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="head-desc">
                          <h3 class="pl-3">
                              Billing
                          </h3>
                          <h2 class="pl-3">
                              Address
                          </h2>
                      </div>
                  </div>
                    <div class="col-md-4 order-md-2 mb-4">
                      <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-dark">Item</span>
                        <span class="badge badge-secondary badge-pill bg-dark"></span>
                      </h4>
                      <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                          <div>
                            <h6 class="my-0">{{ $pkg->title }}</h6>
                            <!-- <small class="text-muted">Brief description</small> -->
                          </div>
                          <span class="text-muted">${{ $pkg->price }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between" id="promo" style="display:none !important">
                            <div>
                                <h6 class="my-0">Promo code</h6>
                                <small id="couponCodeTitle"></small>
                            </div>
                            <span id="couponValue">$5</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                          <span>Total (USD)</span>
                          <strong>$<span id="total">{{$pkg->price}}</span></strong>
                        </li>
                      </ul>

                      <form class="card p-2">
                        <div class="input-group">
                        <input type="text" class="form-control" placeholder="Promo code" id="code">
                        <div class="input-group-append">
                            <button type="button" class="btn  getBtn ml-0" id="codeBtn">Redeem</button>
                        </div>
                        </div>
                    </form>
                    </div>

                    <div class="col-md-8 order-md-1 ">                      
                      <form class="needs-validation" novalidate id="checkoutForm">
                        <div class="row">
                          <div class="col-md-6 mb-3">
                            <label for="firstName">Name</label>
                            <input type="text" class="form-control" value="{{ auth()->user()->name }}" id="name" name="name" required>
                            <small class="error_msg" id="name_msg"></small>
                          </div>

                          <div class="col-md-6 mb-3">
                            <label for="lastName">Email</label>
                            <input type="email" class="form-control" id="email" value="{{ auth()->user()->email }}" name="email_msg" required>
                            <small class="error_msg" id="email_msg"></small>
                          </div>
                        </div>

                        <div class="mb-3">
                          <label for="address">Address</label>
                          <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required>
                          <small class="error_msg" id="address_msg"></small>
                        </div>

                        <hr class="mb-4">

                        <h4 class="mb-3">Payment</h4>
                        <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="cc-name">Name on card</label>
                            <input type="text" class="form-control" id="card_name" name="card_name" placeholder="" required="">
                            <small class="error_msg" id="card_name_msg"></small>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="cc-number">Credit card number</label>
                            <input type="number" class="form-control" id="card_number" name="card_number" placeholder="" required="">
                            <small class="error_msg" id="card_number_msg"></small>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Expiration Month</label>
                            <input type="number" class="form-control" id="expiration_month" name="expiration_month" placeholder="12" required="">
                            <small class="error_msg" id="expiration_month_msg"></small>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Expiration Year</label>
                            <input type="number" class="form-control" id="expiration_year" name="expiration_year" placeholder="2024" required="">
                            <small class="error_msg" id="expiration_year_msg"></small>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-cvv">CVV</label>
                            <input type="number" class="form-control" id="cvv" name="cvv" placeholder="123" required="">
                            <small class="error_msg" id="cvv_msg"></small>
                        </div>
                        </div>
                        <hr class="mb-4">
                        <button class="btn btn-lg btn-block getBtn ml-0" type="submit" id="btnSub">Continue to checkout</button>
                      </form>
                    </div>
                </div>
            </div>
        </section>


@endsection

@section('footer')
<script src="{{ asset('/stripe/stripe.js') }}"></script>
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>

$('#codeBtn').click(function(e){
  e.preventDefault()
  let val = $('#code').val()
  if(val){
    $.ajax({
      url:`{{ url('/coupon-redeem') }}/${val}/{{ Request::segment(2) }}`,
      type:'GET',
      success:function(res){
        if(res.msg == 'success'){
          $('#couponCodeTitle').html(res.title)
          $('#couponValue').html(res.dis)
          $('#total').html(parseFloat(res.total).toFixed(2))
          $('#promo').show(500)
          $('#code').val('')
          $.notify('Coupon Applied', { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
        }
        else{
          $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        }
      }
    });
  }
  else{
    $.notify('Promo Code Field Is Required', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
  }
})


$('#checkoutForm').on('submit',function(e){
  e.preventDefault()
    $('.error_msg').html('')
    if(validate()){
      Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
      Stripe.createToken({
        number: $('#card_number').val(),
        cvc: $('#cvv').val(),
        exp_month: $('#expiration_month').val(),
        exp_year: $('#expiration_year').val()
      }, stripeResponseHandler);
    }
})

function stripeResponseHandler(status, response) {
        if (response.error) {
                $.notify(response.error.message, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
        } else {
            var token = response['id'];
            var name = $('#name').val()
            var email = $('#email').val()
            var address = $('#address').val()
            var formData = new FormData()
            formData.append('name',name)
            formData.append('email',email)
            formData.append('address',address)
            formData.append('package',"{{ Request::segment(2) }}")
            formData.append('stripeToken',token)
            $.ajaxSetup({
                headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            });
            $.ajax({
                url: "{{ url('/checkout') }}",
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function(){
                    $("#btnSub").prop("disabled", true);
                },
                success:function(res){
                    if(res.msg == 'success'){
                      $.notify(res.res, { globalPosition:"top center", autoHideDelay: 7000, className:'success' });
                      setTimeout(() => {
                        window.location.href = "{{ url('/home')}}"
                      },3000)
                    }
                    else{
                      $.notify('Error while placing order', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                    }
                    $("#btnSub").prop("disabled", false);
                },
                error:function(error){
                    $.each(error.responseJSON.errors,function(key,value) {
                    $("#"+key+"_msg").html(value);
                });
                $("#btnSub").prop("disabled", false);
                },
            });

        }
    }



function validate(){
  var cardName = $('#card_name').val()
  var cardNumber = $('#card_number').val()
  var expireMonth = $('#expiration_month').val()
  var expireYear = $('#expiration_year').val()
  var cvv = $('#cvv').val()
  if(cardName == ''){
    $('#card_name_msg').html('Card name is required')
    return false
  }
  else if(cardNumber == ''){
    $('#card_number_msg').html('Card number is required')
    return false
  }
  else if(cardNumber.trim().length !== 16 ){
    $('#card_number_msg').html('Invalid Card Number format')
    return false
  }
  else if(expireMonth == ''){
    $('#expire_month_msg').html('Card expiration month is required')
    return false
  }
  else if(expireYear == ''){
    $('#expire_year_msg').html('Card expiration is required')
    return false
  }
  else if(cvv == ''){
    $('#cvv_msg').html('card cvv number is required')
    return false
  }
  else if(cvv.trim().length < 3){
    $('#cvv_msg').html('Invalid cvv number format')
    return false
  }
  else{
    return true
  }
}
</script>
    
@endsection