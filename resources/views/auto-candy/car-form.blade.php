@extends('layouts.app')

@section('title') Car Register @endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}" />
@endsection

@section('banner')
    
<section class="dynamicHeaderInner hero  advancedBg">
    <div class="container">
       <div class="heading">
          <h3>
             CAR Register
          </h3>
       </div>
    </div>
 </section>

@endsection

@section('content')

<div class="mainBody carFormPage">
    <section class="blogs-news listing-featured-vehicles featured-vehicles pt-0 pb-0">
       <div class="container">
          <div class="row">
             <div class="col-md-12">
                <div class="head-desc">
                   <h3>
                      ADD
                   </h3>
                   <h2>
                      NEW CAR
                   </h2>
                </div>
             </div>
             <div class="col-md-3">
                <div class="addvertisment-box">
                   <img src="./assets/images/add/60months.jpg" alt="">
                   <img class="mt-3" src="./assets/images/add/56months.jpg" alt="">
                </div>
             </div>
             <div class="col-md-9">
                <div class="row">
                   <div class="col-md-12">                             
                      <nav class="advanceFilterTabs">
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Add New Vehicle</a>                                 
                         
                        </div>
                      </nav>

                      @if($type == 'no-vehicle' || $type == 'expire')

                        <div class="row mt-2 packagesDetails packagesDetailsAutomotive">

                            <div class="col-md-12">
                                <div class="alert alert-primary" role="alert"> {{ $msg }} </div>
                            </div>
                            
                            <div class="container">
                            <div class="row text-center packagesDetailsRow">
                                <div class="col-md-3"></div>
                                <div class="col-md-6 packageCol" style="margin-top:0px;">
                                    <h4><span>{{ $pkg->package->title }}</span></h4>
                                    <h3>{{ $pkg->package->package_type == 'Paid' ? '$'.$pkg->package->price : $pkg->package->trial_days.' Days' }}</h3>
                                    <ul class="packageColList mb-4">
                                        <li>{{ $pkg->package->duration == 1 ? 'One Time Only' : 'Once Per Month' }}</li>
                                        <li>{{ $pkg->package->vehicle }} accessorized vehicles</li>
                                        <li>{{ $pkg->package->image }} pictures per vehicle provided by the Automotive Dealer</li>
                                        <li>{{ $pkg->package->video == 0 ? 'No video for vehicle' : $pkg->video.' video link provided by the Automotive Dealer for each vehicle listing.' }}</li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-md-12 text-center mt-4">
                                            <a href="{{ Auth::user()->user_type == 'automotive' ? url('/package/private') : url('/package/private') }}" class="getBtn">Update Package</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>

                        </div>

                      @elseif($type == 'no-package')

                      <div class="row mt-2">

                        <div class="col-md-12 text-center">
                            <div class="alert alert-primary" role="alert"> {{ $msg }} </div>
                        </div>

                        <div class="col-md-12 text-center mt-5">
                            <a href="{{ Auth::user()->user_type == 'automotive' ? url('/package/automotive') : url('/package/private') }}" class="getBtn">{{ Auth::user()->user_type == 'automotive' ? 'Automotive Dealer Packages' : 'Private Seller Packages' }}</a>
                        </div>

                      </div>

                      @else

                      <!-- form div start here -->
                      <div class="tab-content advanceFilterContent" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                          <form id="formSubmit">
                               <div class="col-md-12 pl-0 pr-0 productCategoryCol">
                                  <div class="accordion pl-4 pr-4 pt-4 pb-4" id="accordionExample">
                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered changeIcon">
                                              <div class="card-header" id="headingOne">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseOne">
                                                    Vin Number
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                       <div class="card-body">
                                                         <input type="text" name="vin_number" id="vin_number" class="inputCurve fullWidthInput" placeholder="Vin Number">
                                                         <small class="error_msg" id="vin_number_msg"></small>
                                                       </div>
                                                    </div>
                                                 </div>           
                                              </div>
                                           </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card bordered changeIcon">
                                               <div class="card-header" id="headingOne">
                                                  <h5 class="mb-0">
                                                     <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                        aria-expanded="true" aria-controls="collapseOne">
                                                     Price
                                                     </button>
                                                  </h5>
                                               </div>
                                               <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                                  <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="card-body">
                                                          <input type="number" name="price" id="price" class="inputCurve fullWidthInput" placeholder="price">
                                                          <small class="error_msg" id="price_msg"></small>
                                                        </div>
                                                     </div>
                                                  </div>           
                                               </div>
                                            </div>
                                         </div>

                                     </div>    
                                     
                                     
                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Make
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="make" name="make">
                                                        <option value="0">Select Make</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Make')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="make_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                    Model
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body">
                                                    <input type="text" name="model" id="model" class="inputCurve fullWidthInput" placeholder="Model">
                                                    <small class="error_msg" id="model_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Body Style
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="body_style" name="body_style">
                                                        <option value="0">Select Body Style</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Body Style')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="body_style_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                    Year
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body">
                                                    <input type="number" name="year" id="year" class="inputCurve fullWidthInput" placeholder="1996">
                                                    <small class="error_msg" id="year_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Condition
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="condition" name="condition">
                                                        <option value="0">Select Condition</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Condition')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="condition_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                    Mileage
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body">
                                                    <input type="number" name="mileage" id="mileage" class="inputCurve fullWidthInput" placeholder="95000">
                                                    <small class="error_msg" id="mileage_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                       Transmission
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="transmission" name="transmission">
                                                        <option value="0">Select Transmission</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Transmission')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="transmission_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                    Drivetrain
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="drivetrain" name="drivetrain">
                                                        <option value="0">Select Drivetrain</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Drivetrain')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="drivetrain_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Engine
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="engine" id="engine" class="inputCurve fullWidthInput" placeholder="4S">
                                                    <small class="error_msg" id="engine_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                       Fuel
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="fuel" name="fuel">
                                                        <option value="0">Select Fuel Type</option>
                                                        @foreach($cars as $car)
                                                            @if($car->type == 'Fuel')
                                                                <option value="{{ $car->id }}">{{ $car->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="fuel_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Fuel Economy
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="fuel_economy" id="fuel_economy" class="inputCurve fullWidthInput" placeholder="4">
                                                    <small class="error_msg" id="fuel_economy_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                       Trim
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body">
                                                    <input type="text" name="trim" id="trim" class="inputCurve fullWidthInput" placeholder="Black">
                                                    <small class="error_msg" id="trim_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Exterior Color
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="exterior_color" id="exterior_color" class="inputCurve fullWidthInput" placeholder="White">
                                                    <small class="error_msg" id="exterior_color_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                       Interior Color
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body">
                                                    <input type="text" name="interior_color" id="interior_color" class="inputCurve fullWidthInput" placeholder="Black">
                                                    <small class="error_msg" id="interior_color_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Stock Number
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="number" name="stock_number" id="stock_number" class="inputCurve fullWidthInput" placeholder="8025873567156">
                                                    <small class="error_msg" id="stock_number_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFive">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                       Status
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFive" class="collapse show" aria-labelledby="headingFive">
                                                 <div class="card-body fullCard">
                                                    <select class="inputCurve fullWidthSelect" id="status" name="status">
                                                        <option value="0">Select Status</option>
                                                        <option value="Sold">Sold</option>
                                                        <option value="Unsold">Unsold</option>
                                                    </select>
                                                    <small class="error_msg" id="status_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-12">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Features And Options
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <textarea name="feature_option" id="feature_option" rows="8" class="inputCurve fullWidthInput"></textarea>
                                                    <small class="error_msg" id="feature_option_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <hr/>

                                     <div class="row">

                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Name
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="name" id="name" class="inputCurve fullWidthInput" placeholder="John Doe" value="{{ Auth::user()->name }}">
                                                    <small class="error_msg" id="name_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card bordered ">
                                               <div class="card-header" id="headingFour">
                                                  <h5 class="mb-0">
                                                     <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                        aria-expanded="true" aria-controls="collapseFour">
                                                     Email
                                                     </button>
                                                  </h5>
                                               </div>
                                               <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                  <div class="card-body">
                                                     <input type="text" name="email" id="email" class="inputCurve fullWidthInput" placeholder="example@domain.com" value="{{ Auth::user()->email }}">
                                                     <small class="error_msg" id="email_msg"></small>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>

                                     </div>

                                     <div class="row">

                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Phone
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="number" name="phone" id="phone" class="inputCurve fullWidthInput" value="{{ Auth::user()->phone }}">
                                                    <small class="error_msg" id="phone_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card bordered ">
                                               <div class="card-header" id="headingFour">
                                                  <h5 class="mb-0">
                                                     <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                        aria-expanded="true" aria-controls="collapseFour">
                                                     Zip Code
                                                     </button>
                                                  </h5>
                                               </div>
                                               <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                  <div class="card-body">
                                                     <input type="number" name="zip" id="zip" class="inputCurve fullWidthInput" placeholder="34997" value="{{ Auth::user()->profile()->zip }}">
                                                     <small class="error_msg" id="zip_msg"></small>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>

                                     </div>

                                     <div class="row">

                                        <div class="col-md-12">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    Address
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="address" id="address" class="inputCurve fullWidthInput" placeholder="abc main st" value="{{ Auth::user()->profile()->address }}">
                                                    <small class="error_msg" id="address_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                     </div>

                                     <div class="row">

                                        <div class="col-md-6">
                                           <div class="card bordered ">
                                              <div class="card-header" id="headingFour">
                                                 <h5 class="mb-0">
                                                    <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                       aria-expanded="true" aria-controls="collapseFour">
                                                    City
                                                    </button>
                                                 </h5>
                                              </div>
                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                 <div class="card-body">
                                                    <input type="text" name="city" id="city" class="inputCurve fullWidthInput" placeholder="Boca Raton" value="{{ Auth::user()->profile()->city }}">
                                                    <small class="error_msg" id="city_msg"></small>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card bordered ">
                                               <div class="card-header" id="headingFour">
                                                  <h5 class="mb-0">
                                                     <button class="btn btn-link styleTitle" type="button" data-toggle="collapse" data-target=""
                                                        aria-expanded="true" aria-controls="collapseFour">
                                                     State
                                                     </button>
                                                  </h5>
                                               </div>
                                               <div id="collapseFour" class="collapse show" aria-labelledby="headingFour">
                                                  <div class="card-body fullCard">
                                                     <select class="inputCurve fullWidthSelect" id="state" name="state">
                                                        <option value="0">Select State</option>
                                                        @foreach($states as $key=>$state)
                                                            <option value="{{ $key }}" {{ Auth::user()->profile()->state == $key ? 'selected' : '' }}>{{ $state }}</option>
                                                        @endforeach
                                                    </select>
                                                     <small class="error_msg" id="state_msg"></small>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>

                                     </div>  
                                     <div class="row">
                                        <div class="col-md-12 text-center mt-5 mb-5">
                                           <button type="submit" class="blueBtn pl-5 pr-5 pt-3 pb-3 ml-3 mr-3" id="btnSub"> Submit </button>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                         </form>
                        </div>                                                      
                      </div> 
                      <!-- form div end here -->
                      @endif


                   </div>
                </div>  
             </div>                     
          </div>
       </div>
    </section>
 </div>
    
@endsection

@section('footer')
    
<script src="{{ asset('/script/car-register.js') }}"></script>

@endsection
