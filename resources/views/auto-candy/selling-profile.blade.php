@extends('layouts.app')

@section('title') Home @endsection

@section('header')

<link rel="stylesheet" href="{{ asset('/assets/css/new-style.css') }}">

@endsection

@section('banner')
<section class="dynamicHeaderInner hero  packagesSec">
            <div class="container">
                <div class="heading">
                    <h3>
                        SELL MY CUSTOM VEHICLE
                    </h3>
                </div>                
            </div>
        </section>
@endsection

@section('content')

<div class="mainBody">			
           <section class="sellingProfile blogs-news pt-0 pb-0">
               <div class="container">
                    <div class="row">                        
                        <div class="col-md-12  text-center pl-0">
                            <div class="head-desc">
                                <h3 class="text-center">
                                    SELECT YOUR 
                                </h3>
                                <h2 class="text-center">
                                    SELLING PROFILE
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row sellingProfileRow">
                       <div class="col-md-2"></div>
                       <div class="col-md-4 text-center"><a href="{{ url('/register/private') }}" class="blueBtn ">Private Seller</a></div>
                       <div class="col-md-4 text-center"><a href="{{ url('/register/automotive') }}" class="blueBtn">Automotive Dealer</a></div>
                       <div class="col-md-2"></div>
                    </div>
               </div>
           </section>
		</div>	

@endsection

@section('footer')

@endsection