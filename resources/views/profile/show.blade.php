@extends('layouts.admin')

@section('title') Profile @endsection

@section('header')
    
@endsection

@section('heading')
    @include('layouts.admin-heading',[ 'heading' => ' Profile', 'type' => null  ])
@endsection

@section('content')
    
<div class="section-body">
    <div class="container-fluid">
    @if(Session::has('message'))
    <div class="alert {{ Session::get('alert') }}" role="alert">{{ Session::get('message') }}</div>
    @endif
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card card-profile">
                    <div class="card-body text-center">
                        <img class="card-profile-img" style="max-width:9rem !important" src="{{ $user->image == '' ? asset('/uploads/no-image.png') : asset('/uploads/profile/'.$user->image) }}" alt="" />
                        <h4 class="mb-3">{{ $data->name }}</h4>
                        <ul class="social-links list-inline mb-3 mt-2">
                            @if(isset($user->facebook))
                            <li class="list-inline-item"><a href="{{ $user->facebook }}" title="Facebook" data-toggle="tooltip"><i class="fa fa-facebook"></i></a></li>
                            @endif
                            @if(isset($user->twitter))
                            <li class="list-inline-item"><a href="{{ $user->twitter }}" title="Twitter" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li>
                            @endif
                            <li class="list-inline-item"><a href="mailto:{{ $data->email }}" title="{{ $data->email }}" data-toggle="tooltip"><i class="fa fa-envelope"></i></a></li>
                            <li class="list-inline-item"><a href="tel:{{ $data->phone }}" title="{{ $data->phone }}" data-toggle="tooltip"><i class="fa fa-phone"></i></a></li>
                            @if($user->skype)
                            <li class="list-inline-item"><a href="{{ $user->skype }}" title="@skypename" data-toggle="tooltip"><i class="fa fa-skype"></i></a></li>
                            @endif
                        </ul>
                        @if($user->bio)
                        <p class="mb-4">{{ $user->bio }}</p>                            
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($user->user == Auth::id())
    @include('profile.update')
@endif


@endsection

@section('footer')

@if($user->user == Auth::id())
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script>

    $('#formSubmit').on('submit',function(e){
        e.preventDefault()
        $('.error_msg').html('')
        const formData = new FormData(this)
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            contentType: false,
            cache: false,
            processData: false,
            url: "{{ url('/profile-update') }}",
            method: 'POST',
            data: formData,
            beforeSend:function(){
                $("#btnSub").prop("disabled", true);
            },
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    setTimeout(() => {
                        location.reload(true)
                    }, 1500);
                }
                else{
                    $.notify('Error while updating profile', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
                $("#btnSub").prop("disabled", false);
            },
            error:function(error){
                $.each(error.responseJSON.errors,function(key,value) {
                    $("#"+key+"_msg").html(value);
                });
                    $("#btnSub").prop("disabled", false);
            },
        })
    
    })


    $('#passwordUpdate').on('submit',function(e){
        e.preventDefault()
        $('.error_msg').html('')
        const formData = new FormData(this)
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            contentType: false,
            cache: false,
            processData: false,
            url: "{{ url('/update-password') }}",
            method: 'POST',
            data: formData,
            beforeSend:function(){
                $("#updatePass").prop("disabled", true);
            },
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    setTimeout(() => {
                        location.reload(true)
                    }, 1500);
                }
                else{
                    $.notify('Error while updating password', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
                $("#updatePass").prop("disabled", false);
            },
            error:function(error){
                $.each(error.responseJSON.errors,function(key,value) {
                    $("#"+key+"_msg").html(value);
                });
                    $("#updatePass").prop("disabled", false);
            },
        })
    
    })


</script>

@endif
    
@endsection