<div class="section-body  py-4">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-12">
                        <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-blog-tab" data-toggle="pill" href="#pills-blog" role="tab" aria-controls="pills-blog" aria-selected="true">Update Password</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Profile</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                            <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                        </div>
                                    </div>
                                    <form id="formSubmit">
                                    <div class="card-body">
                                        <div class="row clearfix">
                                            @if(Auth::user()->user_type == 'automotive')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Dealership Name</label>
                                                    <input type="text" class="form-control" placeholder="Bob Motors" name="dealership_name" value="{{ $user->dealership_name }}">
                                                    <small class="error_msg" id="dealership_name_msg"></small>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">{{ Auth::user()->user_type == 'automotive' ? 'Dealer ' : '' }} Name</label>
                                                    <input type="text" class="form-control" placeholder="Bob Johnson" name="name" value="{{ Auth::user()->name }}">
                                                    <small class="error_msg" id="name_msg"></small>
                                                </div>
                                            </div>
                                            @if(Auth::user()->user_type == 'private' || Auth::user()->user_type == 'User')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Fan Name</label>
                                                    @if(isset($user->username))
                                                    <input type="text" class="form-control" disabled readonly value="{{ $user->username }}">
                                                    @else
                                                    <input type="text" class="form-control" placeholder="bob797" name="fan_name" value="{{ $user->username }}">
                                                    <small class="error_msg" id="fan_name_msg"></small>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Cell Phone Number</label>
                                                    <input type="number" class="form-control" placeholder="Phone" name="phone" value="{{ Auth::user()->phone }}">
                                                    <small class="error_msg" id="phone_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Work Phone Number</label>
                                                    <input type="text" class="form-control" placeholder="work phone" name="work_phone_number" value="{{ $user->work_phone_number }}">
                                                    <small class="error_msg" id="work_phone_number_msg"></small>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Email</label>
                                                    <input type="text" class="form-control" disabled readonly value="{{ Auth::user()->email }}">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Preferred Method Of Contact</label>
                                                    <select class="form-control" name="preferred_contact_method">
                                                        <option selected hidden value="0">Select Preferred Contact Method</option>
                                                        <option {{ $user->preferred_contact_method == 'Email' ? 'selected' : '' }}>Email</option>
                                                        <option {{ $user->preferred_contact_method == 'Cell Phone Number' ? 'selected' : '' }}>Cell Phone Number</option>
                                                        <option {{ $user->preferred_contact_method == 'Work Phone Number' ? 'selected' : '' }}>Work Phone Number</option>
                                                    </select>
                                                    <small class="error_msg" id="preferred_contact_method_msg"></small>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Profile Image</label>
                                                    <input type="file" class="form-control" name="profileImage" accept="image/*">
                                                    <small class="error_msg" id="profileImage_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">{{ Auth::user()->user_type == 'automotive' ? 'Dealership ' : '' }} Address</label>
                                                    <input type="text" class="form-control" name="address" placeholder="Address" value="{{ $user->address }}">
                                                    <small class="error_msg" id="address_msg"></small>
                                                </div>
                                            </div>

                                            @if(Auth::user()->user_type == 'private' || Auth::user()->user_type == 'User')
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Favorite Vehicle</label>
                                                    <input type="text" class="form-control" placeholder="Dodge Viper" name="favorite_vehicle" value="{{ $user->favorite_car }}">
                                                    <small class="error_msg" id="favorite_vehicle_msg"></small>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">City</label>
                                                    <input type="text" class="form-control" name="city" placeholder="City" value="{{ $user->city }}">
                                                    <small class="error_msg" id="city_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">State</label>
                                                    <select class="form-control" name="state">
                                                        <option value="0" selected hidden>Select State</option>
                                                        @foreach($states as $key=>$state)
                                                            <option value="{{ $key }}" {{ $user->state == $key ? 'selected' : '' }}>{{ $state }}</option>
                                                        @endforeach
                                                    </select>
                                                    <small class="error_msg" id="state_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Zip Code</label>
                                                    <input type="number" class="form-control" name="zip" placeholder="Zip Code" value="{{ $user->zip }}">
                                                    <small class="error_msg" id="zip_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Skype</label>
                                                    <input type="text" class="form-control" name="skype" placeholder="Skype" value="{{ $user->skype }}">
                                                    <small class="error_msg" id="skype_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Facebook</label>
                                                    <input type="url" class="form-control" name="facebook" placeholder="Facebook URL" value="{{ $user->facebook }}">
                                                    <small class="error_msg" id="facebook_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Twitter</label>
                                                    <input type="url" class="form-control" name="twitter" placeholder="Twitter URL" value="{{ $user->twitter }}">
                                                    <small class="error_msg" id="twitter_msg"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group mb-0">
                                                    <label class="form-label">About Me</label>
                                                    <textarea rows="5" class="form-control" name="aboutMe" placeholder="Here can be your description">{{ $user->bio }}</textarea>
                                                    <small class="error_msg" id="aboutMe_msg"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-right">
                                        <button type="submit" id="btnSub" class="btn btn-primary">Update Profile</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-blog" role="tabpanel" aria-labelledby="pills-blog-tab">
                                <div class="card">
                                    <form id="passwordUpdate">
                                    <div class="card-body">
                                        <div class="new_post">
                                            <div class="form-group">
                                                <label class="form-label">Current Password</label>
                                                <input type="password" class="form-control" name="currentPassword">
                                                <small class="error_msg" id="currentPassword_msg"></small>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">New Password</label>
                                                <input type="password" class="form-control" name="newPassword">
                                                <small class="error_msg" id="newPassword_msg"></small>
                                            </div>
                                            <div class="mt-4 text-right">
                                                <button type="submit" class="btn btn-primary" id="updatePass">Update Password</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>                        
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>