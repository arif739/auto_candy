var editor;

getData()

function getData(val = null){
    url = currentUrl;
    if(val){
        url = `${currentUrl}?type=${val}`
    }

    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          rowReorder: true,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
        ajax:{
            url: url
        },
        columns: [
            // { data: 'order', name: 'order' },
            { data: 'name', name: 'name' },
            { data: 'type', name: 'type' },
            { data: 'created', name: 'created' },
            { data: 'action', name: 'action', orderable: false },
        ]

    });

//     userTable.on('click', 'button', function () {
//         var data = table.row($(this).closest('tr')).data();
//         alert(data[Object.keys(data)[0]]+' s phone: '+data[Object.keys(data)[1]]);
//   });

    userTable.on('row-reorder.dt', function(e, details, edit){ 


        console.log(details[0])
        // for(var i = 0; i < details.length; i++){
            
        // //    console.log(
        // //       'Node', details[i].node, 
        // //       'moved from', details[i].oldPosition, 
        // //       'to', details[i].newPosition
        // //    );
        // }
     });






}


$('#formSubmit').on('submit',function(e){
    e.preventDefault()
    $('.error_msg').html('')
    var btnSub = $("#btnSub").attr('data-type')
    const formData = new FormData(this)
    var url = btnSub == 'create' ? currentUrl : `${currentUrl}/${$('#id').val()}`
    btnSub == 'update' && formData.append('_method','PUT')
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    })
    $.ajax({
        contentType: false,
        cache: false,
        processData: false,
        url: url,
        method: 'POST',
        data: formData,
        beforeSend:function(){
            $("#btnSub").prop("disabled", true);
        },
        success:function(res){
            if(res.msg == 'success'){
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                close()
                getData()
                $('#priceBox').css('display','none')
                $('#trialBox').css('display','none')
                $('#btnSub').attr('data-type','create')
            }
            else{
                $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
            }
            $("#btnSub").prop("disabled", false);
        },
        error:function(error){
            $.each(error.responseJSON.errors,function(key,value) {
                $("#"+key+"_msg").html(value);
            });
                $("#btnSub").prop("disabled", false);
        },
    })

})


$(document).on('click','.edit',function(){
    $('.error_msg').html('')
    var id = $(this).attr('data-id')
    $.ajax({
        url:`${currentUrl}/${id}/edit`,
        type:"GET",
        success:function(res){
            if(res.msg == 'success'){
                var html = ''
                $('#name').val(res.post.name)
                $('#id').val(res.post.id)
                $(`#type option[value='${res.post.type}']`).attr('selected','selected');
                $('#btnSub').attr('data-type','update')
                open()
                window.scrollTo({ top: 100, left: 100, behavior: 'smooth' });
            }
            else{
                $.notify('Unable to fetch record.', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
            }
        }
    })
})


$(document).on('click','.delete',function(){
    var id = $(this).attr('data-id')
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        })
        $.ajax({
            url:`${currentUrl}/${id}`,
            type:"DELETE",
            success:function(res){
                if(res.msg == 'success'){
                    $.notify(res.res, { globalPosition:"top center", autoHideDelay: 5000, className:'success' });
                    getData()
                }
                else{
                    $.notify('Error while deleting car option', { globalPosition:"top center", autoHideDelay: 5000, className:'error' });
                }
            }
        })

        }
    })

})

$('#package_type').change(function(e){
    e.preventDefault();
    let val = $(this).val()
    if(val == 'Paid'){
        $('#priceBox').show(500)
        $('#trialBox').css('display','none')
    }
    else if(val == 'Free Trial'){
        $('#trialBox').show(500)
        $('#priceBox').css('display','none')
    }
    else{
        $('#priceBox').css('display','none')
        $('#trialBox').css('display','none')
    }
});


$('#filter').change(function(e){
    e.preventDefault();
    let val = $(this).val()
    if(val == '1'){
        getData()
    }
    else{
        getData(val)
    }
})
