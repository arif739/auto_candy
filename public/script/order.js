getData()

function getData(val = null){
    url = currentUrl;
    if(val){
        url = `${currentUrl}?type=${val}`
    }

    $('#datatable').DataTable().destroy();
        var userTable = $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          responsive: false,
          rowReorder: true,
          language: {
              search: "",
              searchPlaceholder: "Search records"
         },
        ajax:{
            url: url
        },
        columns: [
            { data: 'order', name: 'order' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'address', name: 'address' },
            { data: 'amount', name: 'amount' },
            { data: 'package', name: 'package' },
            // { data: 'action', name: 'action', orderable: false },
        ]

    });

//     userTable.on('click', 'button', function () {
//         var data = table.row($(this).closest('tr')).data();
//         alert(data[Object.keys(data)[0]]+' s phone: '+data[Object.keys(data)[1]]);
//   });

    userTable.on('row-reorder.dt', function(e, details, edit){ 


        console.log(details[0])
        // for(var i = 0; i < details.length; i++){
            
        // //    console.log(
        // //       'Node', details[i].node, 
        // //       'moved from', details[i].oldPosition, 
        // //       'to', details[i].newPosition
        // //    );
        // }
     });






}

$('#filter').change(function(e){
    e.preventDefault();
    let val = $(this).val()
    if(val == '1'){
        getData()
    }
    else{
        getData(val)
    }
})