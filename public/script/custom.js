$(".fancy-box").fancybox();

$('#open').click(function(e){ e.preventDefault(); open(); })
$('#close').click(function(e){ e.preventDefault(); close(); })

function open(){
    $('#post').show(500)
    $('#close').show(500)
    $('#details').css('display','none')
    $('#open').css('display','none')
}

function close(){
    $('#details').show(500)
    $('#open').show(500)
    $('#post').css('display','none')
    $('#close').css('display','none')
    $('#formSubmit')[0].reset();
    $('textarea').val('')
    $('.imgDiv').css('display','none')
    $('#btnSub').attr('data-type','create')
}

$('.showMore').click(function(){
    let id = $(this).attr('data-id')
    let type = $(this).attr('data-type')
    let less = $(this).attr('data-less')
    let more = $(this).attr('data-content')
    if(type == 'less'){
        $(`#${id}`).attr('data-type','more')
        $(`#content${id}`).html(more)
        $(`#${id}`).html('[See Less]')
    }
    else if(type == 'more'){
        $(`#${id}`).attr('data-type','less')
        $(`#content${id}`).html(less)
        $(`#${id}`).html('[See More]')
    }
})