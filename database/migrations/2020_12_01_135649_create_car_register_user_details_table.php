<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarRegisterUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_register_user_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user');
            $table->integer('car_register_id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('zip');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_register_user_details');
    }
}
