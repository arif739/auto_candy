<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_registers', function (Blueprint $table) {
            $table->id();
            $table->string('vin');
            $table->string('price');
            $table->integer('make');
            $table->string('model');
            $table->string('body_style');
            $table->integer('year');
            $table->string('condition');
            $table->integer('mileage');
            $table->integer('transmission');
            $table->integer('drivetrain');
            $table->string('engine');
            $table->integer('fuel');
            $table->string('fuel_economy');
            $table->string('trim');
            $table->string('exterior_color');
            $table->string('interior_color');
            $table->string('stock_number');
            $table->integer('car_status');
            $table->longText('feature_option');
            $table->integer('status')->default(0)->nullable();
            $table->integer('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_registers');
    }
}
