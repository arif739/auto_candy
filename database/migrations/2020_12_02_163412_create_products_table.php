<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('price')->default(null)->nullable();
            $table->integer('category')->default(null)->nullable();
            $table->integer('sub_category')->default(null)->nullable();
            $table->text('short_description')->default(null)->nullable();
            $table->longText('description')->default(null)->nullable();
            $table->longText('product_info')->default(null)->nullable();
            $table->string('type');
            $table->string('image');
            $table->integer('user');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
