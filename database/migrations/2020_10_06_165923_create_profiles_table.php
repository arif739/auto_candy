<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->text('bio')->default(null)->nullable();
            $table->string('gender')->default(null)->nullable();
            $table->string('address')->default(null)->nullable();
            $table->string('image')->default(null)->nullable();
            $table->string('skype')->default(null)->nullable();
            $table->string('facebook')->default(null)->nullable();
            $table->string('twitter')->default(null)->nullable();
            $table->integer('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
