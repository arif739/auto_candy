<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Roles;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Roles();
        $role_admin->name = 'Admin';
        $role_admin->discription = 'Admin Control All Content';
        $role_admin->save();

        $role_user = new Roles();
        $role_user->name = 'User';
        $role_user->discription = 'Normal User';
        $role_user->save();

        $auto_user = new Roles();
        $auto_user->name = 'automotive';
        $auto_user->discription = 'Automotive Dealer';
        $auto_user->save();

        $private_user = new Roles();
        $private_user->name = 'private';
        $private_user->discription = 'Private Seller';
        $private_user->save();
        
    }
}
