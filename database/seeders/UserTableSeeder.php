<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Roles;
use App\Models\Profile;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role_user = Roles::where('name','User')->first();
        // $role_admin = Roles::where('name','Admin')->first();


        // $admin = new User();
        // $admin->name = 'John Doe';
        // $admin->email = 'admin@mail.com';
        // $admin->email_verified_at = now();
        // $admin->phone = '123456789';
        // $admin->password = bcrypt('12345');
        // $admin->user_type = 'Admin';
        // $admin->status = 1;
        // $admin->save();
        // $admin->roles()->attach($role_admin);

        // $admin_profile = new Profile();
        // $admin_profile->user = $admin->id;
        // $admin_profile->save();

        // $user = new User();
        // $user->name = 'User';
        // $user->email = 'user@mail.com';
        // $user->email_verified_at = now();
        // $user->phone = '1234567890';
        // $user->password = bcrypt('12345');
        // $user->user_type = 'User';
        // $user->status = 1;
        // $user->save();
        // $user->roles()->attach($role_user);

        // $user_profile = new Profile();
        // $user_profile->user = $user->id;
        // $user_profile->save();

        $role_user = Roles::where('name','automotive')->first();
        $role_admin = Roles::where('name','private')->first();


        $admin = new User();
        $admin->name = 'Automotive Dealer';
        $admin->email = 'auto@mail.com';
        $admin->email_verified_at = now();
        $admin->phone = '12345678900';
        $admin->password = bcrypt('12345');
        $admin->user_type = 'automotive';
        $admin->status = 1;
        $admin->save();
        $admin->roles()->attach($role_admin);

        $admin_profile = new Profile();
        $admin_profile->user = $admin->id;
        $admin_profile->save();

        $user = new User();
        $user->name = 'Private Seller';
        $user->email = 'private@mail.com';
        $user->email_verified_at = now();
        $user->phone = '12345678901';
        $user->password = bcrypt('12345');
        $user->user_type = 'private';
        $user->status = 1;
        $user->save();
        $user->roles()->attach($role_user);

        $user_profile = new Profile();
        $user_profile->user = $user->id;
        $user_profile->save();

        
    }
}
