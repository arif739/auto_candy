<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;


Route::get('/logout', function () { Auth::logout(); Session::flush(); return redirect('/home'); });

Route::get('/test',function(){
    return view('Admin.dashboard');
});

Route::group(['middleware' => 'roles','roles' => ['Admin','User','Manager','automotive','private']], function () {
    
    Route::get('/profile/{id?}','App\Http\Controllers\HomeController@profile');
    Route::post('/profile-update','App\Http\Controllers\HomeController@profileUpdate');

    Route::get('/user-order/{status}', [App\Http\Controllers\Admin\AdminController::class, 'userOrder']);
    Route::get('/user-order-details/{id}', [App\Http\Controllers\Admin\AdminController::class, 'userOrderDetails']);

});

Route::group(['middleware' => 'CompleteProfile'], function(){

Route::group(['middleware' => 'roles','roles' => ['Admin','User','Manager','automotive','private']], function () {
    
    Route::post('/update-password','App\Http\Controllers\HomeController@updatePassword');
    Route::get('/car-register', 'App\Http\Controllers\CarRegisterController@index');
    Route::post('/car-register', 'App\Http\Controllers\CarRegisterController@carRegisterPost');
});

Route::group(['prefix' => 'automotive', 'roles' => 'automotive'], function () {
    Route::get('/dashboard',function(){
        return view('Admin.dashboard');
   });
});

Route::group(['prefix' => 'user', 'roles' => 'User'], function () {
    Route::get('/dashboard',function(){
        return view('Admin.dashboard');
   });
});

Route::group(['prefix' => 'private', 'roles' => 'private'], function () {
   Route::get('/dashboard',function(){
        return view('Admin.dashboard');
   });
});

Route::group(['middleware' => 'roles','roles' => 'Admin'], function() {

    Route::group(['prefix' => 'admin'],function(){
        Route::get('/dashboard', 'App\Http\Controllers\Admin\AdminController@dashboard');
        Route::resource('/coupon', 'App\Http\Controllers\Admin\CouponController')->name('*','coupon');
        Route::get('/orders', 'App\Http\Controllers\Admin\AdminController@orders');

        Route::get('/getSubCategory/{id}', [App\Http\Controllers\Admin\ProductController::class, 'getsubCategory']);
        Route::resource('/categories', 'App\Http\Controllers\Admin\CategoryController')->name('*','categories');
        Route::resource('/sub-categories', 'App\Http\Controllers\Admin\SubCategoryController')->name('*','sub-categories');
        Route::resource('/product', 'App\Http\Controllers\Admin\ProductController')->name('*','product');
        Route::resource('/coupon', 'App\Http\Controllers\Admin\CouponController')->name('*','coupon');
        Route::get('/order/{status}', [App\Http\Controllers\Admin\AdminController::class, 'userOrder']);
        Route::get('/order-details/{id}', [App\Http\Controllers\Admin\AdminController::class, 'userOrderDetails']);
        Route::get('/order-status-change/{id}/{val}','App\Http\Controllers\Admin\AdminController@orderStatusChange');
        

    });
    
    Route::resource('car-option', App\Http\Controllers\Admin\CarOptionController::class)->name('*', 'carOption');
    Route::resource('packages/automotive', App\Http\Controllers\Admin\PackagesController::class)->name('*', 'package');
    Route::resource('packages/private', App\Http\Controllers\Admin\PackagesController::class)->name('*', 'package');

});

Route::group(['prefix' => 'manager'], function() {

    Route::group(['middleware' => 'roles','roles' => 'Manager'],function(){
        Route::get('/dashboard','App\Http\Controllers\Admin\AdminController@dashboard');
    });

});

Route::group(['prefix' => 'user'], function() {

    Route::group(['middleware' => 'roles','roles' => 'User'],function(){
        Route::get('/user/dashboard','App\Http\Controllers\Admin\AdminController@dashboard');
    });

});


Route::group(['prefix' => '/'], function() {

    Route::get('/',function(){ 
        if(Auth::check()){
            if(Auth::user()->user_type == 'Admin'){ return redirect('/admin/dashboard'); }
            else{ return redirect('/home'); }
        }
        else{
            return redirect('/home');
        }
    });
    Route::get('/home','App\Http\Controllers\HomeController@index');
    Route::get('/package/{type}','App\Http\Controllers\HomeController@package');
    Route::get('/selling-profile','App\Http\Controllers\HomeController@sellingProfile');
    Route::get('/products', [App\Http\Controllers\HomeController::class, 'products'])->name('products');
    Route::get('/product/{id}', [App\Http\Controllers\HomeController::class, 'productDetails']);
    Route::resource('/cart', 'App\Http\Controllers\CartController')->name('*','cart');
    Route::get('/get-cart-count',function(){
        return response()->json(['cartCount' => session()->has('cart') ? count(session()->get('cart')) : 0]);
    });
    

    Route::group(['middleware' => 'guest'], function() {

        Route::get('/email-verified/{token}/{email}', [App\Http\Controllers\Auth\VerificationController::class, 'emailVerified'])->name('email-verification');

    });

    Route::group(['middleware' => 'auth'], function () {
        //Package checkout start here
        Route::get('/checkout/{id}','App\Http\Controllers\HomeController@checkoutPackage');
        Route::post('/checkout','App\Http\Controllers\HomeController@checkoutPost');
        //Package checkout end here

        //Ecommerce checkout start here
        Route::get('/product-checkout', [App\Http\Controllers\CartController::class, 'checkout'])->name('checkout');
        Route::post('/product-checkout', [App\Http\Controllers\CartController::class, 'checkoutStore'])->name('checkout-store');
        //Ecommerce checkout end here

        Route::get('/coupon-redeem/{val}/{id}',[App\Http\Controllers\HomeController::class, 'couponRedeem'])->name('couponRedeem');
        Route::get('/coupon-redeem/{val}',[App\Http\Controllers\HomeController::class, 'couponRedeemProduct'])->name('couponRedeemProduct');
    });

});


});

Auth::routes();
Route::get('/register/{type}','App\Http\Controllers\Auth\RegisterController@customRegister');


