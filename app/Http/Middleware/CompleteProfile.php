<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use Auth;
use Session;

class CompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->user_type == 'Admin'){
                return $next($request);
            }
            elseif(Auth::user()->user_type == 'automotive'){
                $pro = Auth::user()->profile();
                if(Auth::user()->isAutomotiveProfileComplete()){
                    return $next($request);
                }
                else{
                    Session::flash('message','please complete your profile to continue using autocandy.com');
                    Session::flash('alert','alert-info');
                    return redirect('/profile');
                }
            }
            else{
                if(Auth::user()->isProfileComplete()){
                    return $next($request);
                }
                else{
                    Session::flash('message','please complete your profile to continue using autocandy.com');
                    Session::flash('alert','alert-info');
                    return redirect('/profile');
                }
            }
        }
        else{
            return $next($request);
        }
    }
}
