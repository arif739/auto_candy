<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Session;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user() == null){
            return back();
        }
        $actions = $request->route()->getAction();
        $roles = (isset($actions['roles'])) ? $actions['roles'] : null;
        if($request->user()->hasAnyRole($roles) || !$roles){
            if($request->user()->status == 0){
                Session::flash('message','You\'r account has been disabled by admin');
                Session::flash('alert','alert-danger');
                Auth::logout();
                return redirect('/login');
            }
            return $next($request);
        }
        return back();
    }
}
