<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\SubCategory;
use App\Models\Category;
use Auth;

class SubCategoryController extends Controller
{
    
    public function index()
    {
        if(request()->ajax()){
            $tbl = SubCategory::with(['categories'])->where(['type' => 'product'])->get();
            return datatables()->of($tbl)
            ->addColumn('image', function($data){
                $img = asset('/uploads/sub-category/'.$data->image);
                return '<a href="'.$img.'" data-fancybox="images" data-caption="'.$data->name.'" class="fancy-box"><img src="'.$img.'" style="width:100px; height:100px; border-radius:5px" /></a>';
            })
            ->addColumn('name', function($data){
                return $data->name;
            })
            ->addColumn('category', function($data){
                return $data->categories->name;
            })
            ->addColumn('description', function($data){
                $div = '<div><span id="content'.$data->id.'">'.substr($data->description,0,80).'</span> <br>
                <a href="javascript:void(0)" class="showMore" id="'.$data->id.'" data-id="'.$data->id.'" data-type="less" data-content="'.$data->description.'" data-less="'.substr($data->description,0,80).'"> [See more]</a></div>';
                return $div;
            })
            ->addColumn('date', function($data){
                return $data->created_at->format('M d Y');
            })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            ->rawColumns(['image','description','action'])
            ->make(true); 
        }
        $categories = Category::where('type','product')->get();
        return view('Admin.sub-categories',[ 'categories' => $categories ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => ['required','max:255',Rule::unique('sub_categories')->where(function ($query) use ($req) {
                return $query->where('type', $req->type); }) ],
            'category' => 'required|not_in:0',
            'image' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg',
            'description' => 'required|min:50|max:250'
        ]);

        $cat = new SubCategory();
        $cat->name = $req->name;
        $cat->category = $req->category;
        $cat->description = $req->description;
        $cat->type = $req->type;
        $cat->user = Auth::id();
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/sub-category'), $filename);
            $cat->image = $filename;
        }
        if($cat->save()){
            return response()->json(['msg' => 'success', 'res' => 'Sub Category created successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while creating sub category.']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $post = SubCategory::where('id',$id)->first();
        $post->image = asset('/uploads/sub-category/'.$post->image);
        if(isset($post)){
            return response()->json(['msg' => 'success', 'post' => $post]);
        }
    }

    public function update(Request $req, $id)
    {
        $id = isset($id) ? $id : $req->id;
        $req->validate([
            'name' => ['required','max:255',Rule::unique('categories')->where(function ($query) use ($req,$id) {
                return $query->where('type', $req->type)->where('id','!=',$id); }) ],
            'category' => 'required|not_in:0',
            'description' => 'required|min:50|max:250'
        ]);

        if($req->hasFile('image')){ $req->validate([ 'image' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg', ]); }

        $update['name'] = $req->name;
        $update['description'] = $req->description;
        $update['category'] = $req->category;
        $update['type'] = $req->type;
        $update['user'] = Auth::id();
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/sub-category'), $filename);
            $update['image'] = $filename;
        }
        if(SubCategory::where('id',$id)->update($update)){
            return response()->json(['msg' => 'success', 'res' => 'Sub Category Update Successfully']); 
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while updating sub category']);
        }
    }

    public function destroy($id)
    {
        if(SubCategory::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => 'Sub Category Deleted Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while deleting sub category']);
        }
    }
}
