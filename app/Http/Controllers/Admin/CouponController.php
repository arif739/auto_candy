<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Coupon;
use Carbon\Carbon;
use Auth;

class CouponController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $tbl = Coupon::where(['type' => 'product'])->get();
            return datatables()->of($tbl)
            ->addColumn('title', function($data){
                return $data->title;
            })
            ->addColumn('expire', function($data){
                return Carbon::parse($data->expiry_date)->format('M d Y');
            })
            ->addColumn('code', function($data){
                return $data->code;
            })
            ->addColumn('type', function($data){
                return $data->coupon_type;
            })
            ->addColumn('value', function($data){
                return $data->coupon_type == 'Percent' ? $data->value.'%' : '$ '.$data->value;
            })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            ->rawColumns(['action'])
            ->make(true); 
        }
        return view('Admin.coupon');
    }

    public function create()
    {
        //
    }

    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3|max:190',
            'code' => ['required','max:90','min:3',Rule::unique('coupons')->where(function ($query) use ($req) {
                return $query->where('type', $req->type); }) ],
            'expiry_date' => 'required',
            'coupon_type' => 'required|not_in:0',
            'value' => 'required|min:1',
        ]);

        $cou = new Coupon();
        $cou->title = $req->title;
        $cou->code = $req->code;
        $cou->expiry_date = $req->expiry_date;
        $cou->coupon_type = $req->coupon_type;
        $cou->value = $req->value;
        $cou->type = $req->type;
        $cou->user = Auth::id();
        if($cou->save()){
            return response()->json(['msg' => 'success', 'res' => 'Coupon created successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while creating coupon.']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $post = Coupon::where('id',$id)->first();
        $post->expiry_date = Carbon::parse($post->expiry_date)->format('Y-m-d');
        if(isset($post)){
            return response()->json(['msg' => 'success', 'post' => $post]);
        }
    }

    public function update(Request $req, $id)
    {
        $id = isset($id) ? $id : $req->id;
        $req->validate([
            'title' => 'required|min:3|max:190',
            'code' => 'required|min:3|max:90',
            'code' => ['required','min:3','max:90',Rule::unique('coupons')->where(function ($query) use ($req,$id) {
                return $query->where('type', $req->type)->where('id','!=',$id); }) ],
            'expiry_date' => 'required',
            'coupon_type' => 'required|not_in:0',
            'value' => 'required|min:1',
        ]);

        $update['title'] = $req->title;
        $update['code'] = $req->code;
        $update['expiry_date'] = $req->expiry_date;
        $update['coupon_type'] = $req->coupon_type;
        $update['value'] = $req->value;
        $update['type'] = $req->type;
        $update['user'] = Auth::id();
        if(Coupon::where('id',$id)->update($update)){
            return response()->json(['msg' => 'success', 'res' => 'Coupon Update Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while updating coupon']);
        }
    }

    public function destroy($id)
    {
        if(Coupon::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => 'Coupon Deleted Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while deleting coupon']);
        }
    }
}
