<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CarOption;
use Auth;

class CarOptionController extends Controller
{
    public function index()
    {

        if(request()->ajax()){
            if(!empty(request()->get('type'))){
                $tbl = CarOption::where(['type' => request()->get('type')])->get();    
            }
            else{
                $tbl = CarOption::get();
            }
            return datatables()->of($tbl)
            ->addColumn('name', function($data){
                return $data->name;
            })
            ->addColumn('type', function($data){
                return $data->type;
            })
            ->addColumn('created', function($data){
                return $data->created_at->format('M d Y');
            })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            ->rawColumns([])
            ->make(true);
        }

        return view('Admin.car-option');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'name' => 'required|min:1|max:190',
            'type' => 'required|not_in:0'
        ]);

        $opt = new CarOption();
        $opt->name = $req->name;
        $opt->type = $req->type;
        $opt->user = Auth::id();
        if($opt->save()){
            return response()->json(['msg' => 'success', 'res' => 'Car Option Added Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error While Adding Car Option']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opt = CarOption::where('id',$id)->first();
        if(isset($opt)){
            return response()->json(['msg' => 'success', 'post' => $opt]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $id = isset($req->id) ? $req->id : $id;
        $req->validate([
            'name' => 'required|min:1|max:190',
            'type' => 'required|not_in:0'
        ]);

        $update['name'] = $req->name;
        $opt['type'] = $req->type;
        $opt['user'] = Auth::id();
        if(CarOption::where('id',$id)->update($update)){
            return response()->json(['msg' => 'success', 'res' => 'Car Option Updated Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error While Updating Car Option']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(CarOption::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => 'Car Option Deleted Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error While Deleting Car Option']);
        }
    }
}
