<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\User;
use Auth;

class PackagesController extends Controller
{   
    private $type;

    public function __construct(){
        $this->type = request()->segment(2);
    }
    
    public function index()
    {   
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);

        if(request()->ajax()){
            $tbl = Package::where(['type' => $this->type])->get();
            return datatables()->of($tbl)
            // ->addColumn('order', function($data){
            //     return $data->order;
            // })
            ->addColumn('title', function($data){
                return $data->title;
            })
            ->addColumn('duration', function($data){
                if($data->duration == 1){
                    return 'One Time Only';
                }
                else{
                    return 'Once Per Month';
                }
            })
            ->addColumn('type', function($data){
                return $data->package_type;
            })
            ->addColumn('price', function($data){
                if($data->price){
                    return '$'.$data->price;
                }
                else{
                    return '-';
                }
            })
            ->addColumn('trial', function($data){
                if($data->trial_days){
                    return $data->trial_days.' Days';
                }
                else{
                    return '-';
                }
            })
            ->addColumn('vehicle', function($data){
                return $data->vehicle.' vehicle';
            })
            ->addColumn('image', function($data){
                return $data->image.' pictures for vehicle';
            })
            ->addColumn('video', function($data){
                if($data->video == 0){
                    return 'No video for vehicle';    
                }
                return $data->video.' video for vehicle';
            })
            // ->addColumn('created', function($data){
            //     return $data->created_at->format('M d Y');
            // })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            ->rawColumns([])
            ->make(true);
        }

        return view('Admin.packages');
    }

    public function create()
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);
    }

    public function store(Request $req)
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);

        $req->validate([
            'title' => 'required|string|min:3|max:190',
            'duration' => 'required|not_in:0|min:1|max:190',
            'package_type' => 'required|not_in:0|max:190',
            'vehicle' => 'required|max:190',
            'picture' => 'required|max:190',
            'video' => 'required|max:190',
        ]);

        $pkg = new Package();
        $pkg->title = $req->title;
        $pkg->duration = $req->duration;
        $pkg->package_type = $req->package_type;

        if($req->package_type == 'Paid' || $req->package_type == 'Free Trial'){
            if($req->package_type == 'Paid'){
                $req->validate(['price' => 'required']);
                $pkg->price = $req->price;  
            }

            if($req->package_type == 'Free Trial'){
                $req->validate(['trial_days' => 'required|numeric']);
                $pkg->trial_days = $req->trial_days;  
            }

        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid Package Type']);
        }
        
        // $package = Package::orderBy('id','DESC')->first();
        // $order = isset($package) ? (int)$package->order + 1 : $package->order;

        
        $pkg->price = $req->price;
        $pkg->order = 1;//$order;
        $pkg->vehicle = $req->vehicle;
        $pkg->image = $req->picture;
        $pkg->video = $req->video;
        $pkg->type = $this->type;
        $pkg->user = Auth::id();
        if($pkg->save()){
            return response()->json(['msg' => 'success', 'res' => 'Package Created Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error While Creating Package']);
        }


    }

    public function show($id)
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);
    }

    public function edit($id)
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);
        $pkg = Package::where('id',$id)->first();
        if(isset($pkg)){
            return response()->json(['msg' => 'success', 'post' => $pkg]);
        }
    }

    public function update(Request $req, $id)
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);
        $id = isset($id) ? $id : $req->id;

        $req->validate([
            'title' => 'required|string|min:3|max:190',
            'duration' => 'required|not_in:0|min:1|max:190',
            'package_type' => 'required|not_in:0|max:190',
            'vehicle' => 'required|max:190',
            'picture' => 'required|max:190',
            'video' => 'required|max:190',
        ]);

        if($req->package_type == 'Paid' || $req->package_type == 'Free Trial'){
            if($req->package_type == 'Paid'){
                $req->validate(['price' => 'required']);
                $update['price'] = $req->price; 
            }

            if($req->package_type == 'Free Trial'){
                $req->validate(['trial_days' => 'required|numeric']);
                $update['trial_days'] = $req->trial_days;  
            }

        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid Package Type']);
        }

        $update['title'] = $req->title;
        $update['duration'] = $req->duration;
        $update['package_type'] = $req->package_type;
        $update['vehicle'] = $req->vehicle;
        $update['image'] = $req->picture;
        $update['video'] = $req->video;
        $update['user'] = auth()->id();
        if(Package::where('id',$id)->update($update)){
            return response()->json(['msg' => 'success', 'res' => 'Package Updated Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error While Updating Package']);
        }

    }

    public function destroy($id)
    {
        $this->type == 'automotive' || $this->type == 'private' ? '' : abort(404);
        if(Package::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => 'Package Deleted Successfully']); 
        }
    }
}
