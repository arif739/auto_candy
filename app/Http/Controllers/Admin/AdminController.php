<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Models\Post;
use App\Models\Image;
use App\Models\User;
use App\Models\CheckOut;
use App\Models\UserOrder;
use App\Models\UserOrderDetails;
use App\Notifications\OrderUpdate;
use Carbon\Carbon;
use Auth;
use Crypt;

class AdminController extends Controller
{
    public function dashboard(){
        return view('Admin.dashboard');
    }

    public function orders(){

        if(request()->ajax()){
            if(!empty(request()->get('type'))){
                $tbl = CheckOut::with(['packages'])->whereHas('users',function($q){
                    return $q->where('user_type',request()->get('type'));
                })->get();   
            }
            else{
                $tbl = CheckOut::with(['packages'])->get();
            }
            
            return datatables()->of($tbl)
            ->addColumn('order', function($data){
                return '#'.$data->id;
            })
            ->addColumn('name', function($data){
                return $data->name;
            })
            ->addColumn('email', function($data){
                return $data->email;
            })
            ->addColumn('address', function($data){
                return $data->address;
            })
            ->addColumn('amount', function($data){
                return '$'.$data->total;
            })
            ->addColumn('package', function($data){
                return $data->packages->title;
            })
            ->rawColumns([])
            ->make(true);
        }

        return view('Admin.orders');
    }


    public function userOrder($type){
        $status = 0;
        if($type == 'pending'){ $status = 0; }
        elseif($type == 'processing'){ $status = 1; }
        elseif($type == 'completed'){ $status = 2; }
        elseif($type == 'cancel'){ $status = 3; }

        if(request()->ajax()){
            $tbl = UserOrder::where(['order_status' => $status, 'user' => Auth::id()])->get();
            if(Auth::user()->user_type == 'Admin'){
                $tbl = UserOrder::where(['order_status' => $status])->get();
            }
            else{
                $tbl = UserOrder::where(['order_status' => $status, 'user' => Auth::id()])->get();
            }
            return datatables()->of($tbl)
            ->addColumn('name', function($data){
                $url = url('/admin/order-details/'.Crypt::encrypt($data->id));
                return '<a href="'.$url.'">'.$data->name.'</a>';
            })
            ->addColumn('email', function($data){
                return $data->email;
            })
            ->addColumn('address', function($data){
                return $data->address;
            })
            ->addColumn('country', function($data){
                return $data->country;
            })
            ->addColumn('total', function($data){
                return '$'.round($data->total,2);
            })
            ->addColumn('date', function($data){
                return $data->created_at->format('M d Y');
            })
            ->addColumn('action', function($data) use($status) {
                $select = '';
                if(Auth::user()->user_type == 'Admin'){
                    $select .= '<select data-id="'.$data->id.'" class="form-control status">';
                    $select .= '<option value="4" selected hidden>Select Status</option>';
                    $select .= $status == 0 ? '' : '<option value="0">Pending</option>';
                    $select .= $status == 1 ? '' : '<option value="1">Processing</option>';
                    $select .= $status == 2 ? '' : '<option value="2">Completed</option>';
                    $select .= $status == 3 ? '' : '<option value="3">Cancel</option>';
                    $select .= '</select>';
                }
                else{
                    $url = url('/admin/order-details/'.Crypt::encrypt($data->id));
                    $select .= '<a href="'.$url.'">Details</a>';
                }
                return $select;
            })
            ->rawColumns(['name', 'action'])
            ->make(true);
        }

        return view('Admin.order');
    }

    public function userOrderDetails($id){
        $id = Crypt::decrypt($id);
        $order = UserOrder::where('id',$id)->first();
        $products = UserOrderDetails::where('order_id',$id)->get();
        return view('Admin.order-details',['order' => $order, 'products' => $products]);
    }

    public function orderStatusChange($id,$val){
        if(UserOrder::where('id',$id)->update(['order_status' => $val])){
            $user = UserOrder::where('id',$id)->first();
            $email = $user->email;

            $status = '';
            if($val == 0){ $status = 'Pending'; }
            elseif($val == 1){ $status = 'Processing'; }
            elseif($val == 2){ $status = 'Completed'; }
            elseif($val == 3){ $status = 'Canceled'; }

            $when = Carbon::now()->addSeconds(10);
            $noti = Notification::route('mail',$email)->notify((new OrderUpdate($status))->delay($when));

            return 1;
        }
    }

}
