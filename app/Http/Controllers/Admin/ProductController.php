<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Image;
use Auth;
use Crypt;

class ProductController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $tbl = Product::with(['categories','subCategories'])->where(['type' => 'product'])->get();
            return datatables()->of($tbl)
            ->addColumn('image', function($data){
                $img = asset('/uploads/product/'.$data->image);
                return '<a href="'.$img.'" data-fancybox="images" data-caption="'.$data->name.'" class="fancy-box"><img src="'.$img.'" style="width:100px; height:100px; border-radius:5px" /></a>';
            })
            ->addColumn('name', function($data){
                return '<a href="'.url('/admin/product/'.Crypt::encrypt($data->id)).'">'.$data->name.'</a>';
            })
            ->addColumn('price', function($data){
                return '$'.$data->price;
            })
            ->addColumn('category', function($data){
                return $data->categories->name;
            })
            ->addColumn('sub_category', function($data){
                return $data->subCategories->name;
            })
            ->addColumn('date', function($data){
                return $data->created_at->format('M d Y');
            })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            ->rawColumns(['image','name','description','action'])
            ->make(true); 
        }
        $categories = Category::where('type','product')->get();
        return view('Admin.product', ['categories' => $categories]);
    }

    public function create()
    {
        $id =  request()->get('id');
        $image = Image::where('id',$id)->first();
        $path = public_path('/uploads/images/'.$image->image);
        unlink($path);
        $image->delete();
        return response()->json(['msg' => 'success']);
        
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => 'required|min:3|max:190',
            'price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/',
            'category' => 'required|not_in:0',
            'sub_category' => 'required|not_in:0',
            'quantity' => 'required|numeric',
            'min_quantity' => 'required|numeric',
            'max_quantity' => 'required|numeric',
            'short_description' => 'required|min:50|max:250',
            'description' => 'required',
            'product_info' => 'required',
            'image' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if($req->hasFile('product_images')){ $req->validate([ 'product_images.*' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg' ]); }

        $pro = new Product();
        $pro->name = $req->name;
        $pro->price = $req->price;
        $pro->category = $req->category;
        $pro->sub_category = $req->sub_category;
        $pro->short_description = $req->short_description;
        $pro->description = $req->description;
        $pro->product_info = $req->product_info;
        $pro->type = $req->type;
        $pro->quantity = $req->quantity;
        $pro->min_quantity = $req->min_quantity;
        $pro->max_quantity = $req->max_quantity;
        $pro->user = Auth::id();
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/product'), $filename);
            $pro->image = $filename;
        }
        if($pro->save()){
            $id = $pro->id;
            if($req->hasFile('product_images')){
                $files = $req->file('product_images');
                foreach ($files as $item) {
                    $ext = $item->getClientOriginalExtension();
                    $name = $item->getFilename();
                    $name = md5(rand()).'.'.$ext;
                    $item->move(public_path('uploads/images'), $name);
                    Image::create([
                        'image' => $name,
                        'type' => 'product',
                        'post_id' => $id
                    ]);
                }

            }
            return response()->json(['msg' => 'success', 'res' => 'Product Created Successfully.']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while creating product.']);
        }
    }

    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $product = Product::with(['categories'])->where(['id' => $id])->first();
        $product->image = asset('/uploads/product/'.$product->image);
        $images = Image::where(['post_id' => $product->id, 'type' => 'product'])->get();
        return view('Admin.product-details',['product' => $product, 'images' => $images]);
    }

    public function edit($id)
    {
        $product = Product::with(['categories'])->where(['id' => $id])->first();
        $product->image = asset('/uploads/product/'.$product->image);
        $images = Image::where(['post_id' => $product->id, 'type' => 'product'])->get();
        $sub = SubCategory::where(['category' => $product->category, 'type' => 'product'])->get();
        return response()->json(['msg' => 'success', 'product' => $product, 'images' => $images, 'sub' => $sub]);
    }

    public function update(Request $req, $id)
    {
        $id = isset($id) ? $id : $req->id;
        $req->validate([
            'name' => 'required|min:3|max:190',
            'price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/',
            'category' => 'required|not_in:0',
            'sub_category' => 'required|not_in:0',
            'quantity' => 'required|numeric',
            'min_quantity' => 'required|numeric',
            'max_quantity' => 'required|numeric',
            'short_description' => 'required|min:50|max:250',
            'description' => 'required',
            'product_info' => 'required',
        ]);
        if($req->hasFile('image')){ $req->validate([ 'image' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg' ]); }
        if($req->hasFile('product_images')){ $req->validate([ 'product_images.*' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg' ]); }

        $update['name'] = $req->name;
        $update['price'] = $req->price;
        $update['category'] = $req->category;
        $update['sub_category'] = $req->sub_category;
        $update['short_description'] = $req->short_description;
        $update['description'] = $req->description;
        $update['product_info'] = $req->product_info;
        $update['type'] = $req->type;
        $update['quantity'] = $req->quantity;
        $update['min_quantity'] = $req->min_quantity;
        $update['max_quantity'] = $req->max_quantity;
        $update['user'] = Auth::id();
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/product'), $filename);
            $update['image'] = $filename;
        }
        if(Product::where('id',$id)->update($update)){
            if($req->hasFile('product_images')){
                $files = $req->file('product_images');
                foreach ($files as $item) {
                    $ext = $item->getClientOriginalExtension();
                    $name = $item->getFilename();
                    $name = md5(rand()).'.'.$ext;
                    $item->move(public_path('uploads/images'), $name);
                    Image::create([
                        'image' => $name,
                        'type' => 'product',
                        'post_id' => $id
                    ]);
                }

            }
            return response()->json(['msg' => 'success', 'res' => 'Product Created Successfully.']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while creating product.']);
        }
    }

    public function destroy($id)
    {
        if(Product::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => 'Product Deleted Successfully']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while deleting product']);
        }
    }

    public function getsubCategory($id){
        $cat = SubCategory::where(['category' => $id])->get();
        if(count($cat) > 0){
            return response()->json(['msg' => 'success', 'data' => $cat]);
        }
    }
}
