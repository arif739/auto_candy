<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Category;
use App\Models\Image;
use Auth;

class CategoryController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $type = request()->get('type');
            $status = 'product';
            if($type == 'style'){ $status = 'style'; }
            elseif($type == 'room'){ $status = 'room'; }
            elseif($type == 'package'){ $status = 'package'; }
            elseif($type == 'portfolio'){ $status = 'portfolio'; }
            else{ $status == 'product'; }
            if($type == 'portfolio'){
                $tbl = Category::where(['type' => $status, 'user' => Auth::id()])->get();
            }
            else{
                $tbl = Category::where(['type' => $status])->get();
            }
            return datatables()->of($tbl)
            ->addColumn('image', function($data){
                $img = asset('/uploads/category/'.$data->image);
                return '<a href="'.$img.'" data-fancybox="images" data-caption="'.$data->name.'" class="fancy-box"><img src="'.$img.'" style="width:100px; height:100px; border-radius:5px" /></a>';
            })
            ->addColumn('name', function($data){
                return $data->name;
            })
            ->addColumn('description', function($data){
                $div = '<div><span id="content'.$data->id.'">'.substr($data->description,0,80).'</span> <br>
                <a href="javascript:void(0)" class="showMore" id="'.$data->id.'" data-id="'.$data->id.'" data-type="less" data-content="'.$data->description.'" data-less="'.substr($data->description,0,80).'"> [See more]</a></div>';
                return $div;
            })
            ->addColumn('price', function($data){
                return '$'.$data->price;
            })
            ->addColumn('discount_price', function($data){
                return '$'.$data->discount_price;
            })
            ->addColumn('date', function($data){
                return $data->created_at->format('M d Y');
            })
            ->addColumn('images', function($data) use($type, $status){
                $images = Image::where(['post_id' => $data->id, 'type' => $status])->get();
                $sel = '';
                if(count($images) > 0){
                    foreach($images as $img){
                        $sel .= '<a href="'.url('uploads/images/'.$img->image).'" data-fancybox="images" data-caption="'.$data->name.'" class="fancy-box">View</a><br>';
                    }
                }
                return $sel;
            })
            ->addColumn('action', function($data){
                $select = '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon edit" title="Edit details"><i class="la la-edit"></i></a>';
                $select .= '<a href="javascript:;" data-id="'.$data->id.'" class="btn btn-sm btn-clean btn-icon delete" title="Delete"><i class="la la-trash"></i></a>';
                return $select;
            })
            // ->removeColumn($type !== 'style' || $type !== 'portfolio' ? 'images' : '')
            ->removeColumn($type !== 'package' ? 'price' : '')
            ->removeColumn($type !== 'package' ? 'discount_price' : '')
            ->rawColumns(['image','description','action','images'])
            ->make(true);
        }
        return view('Admin.categories');
    }

    public function create()
    {
        $id =  request()->get('id');
        $image = Image::where('id',$id)->first();
        $path = public_path('/uploads/images/'.$image->image);
        unlink($path);
        $image->delete();
        return response()->json(['msg' => 'success']);
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => ['required','max:255',Rule::unique('categories')->where(function ($query) use ($req) {
                return $query->where('type', $req->type); }) ],
            'image' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg',
            'description' => 'required|min:50|max:1000'
        ]);
        if($req->type == 'package'){
            $req->validate([
                'price' => 'required',
                'discount_price' => 'required'
            ]);
        }

        $cat = new Category();
        $cat->name = $req->name;
        $cat->description = $req->description;
        $cat->type = $req->type;
        $cat->user = Auth::id();
        if($req->type == 'package'){
            $cat->price = $req->price;
            $cat->discount_price = $req->discount_price;
        }
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/category'), $filename);
            $cat->image = $filename;
        }
        $msg = '';
        if($req->type == 'style'){ $msg == 'Style'; }
        elseif($req->type == 'room'){ $msg == 'Room'; }
        elseif($req->type == 'package'){ $msg == 'Package'; }
        elseif($req->type == 'portfolio'){ $msg == 'Portfolio'; }
        else{ $msg == 'Product'; }
        if($cat->save()){

            $id = $cat->id;
            if($req->type == 'style' || $req->type == 'portfolio'){
                if($req->style_images){
                    $files = $req->file('style_images');
                    foreach ($files as $item) {
                        $ext = $item->getClientOriginalExtension();
                        $name = $item->getFilename();
                        $name = md5(rand()).'.'.$ext;
                        $item->move(public_path('uploads/images'), $name);
                        Image::create([
                            'image' => $name,
                            'type' => $req->type,
                            'post_id' => $id
                        ]);
                    }
                }
            }

            return response()->json(['msg' => 'success', 'res' => "$msg created successfully"]);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => "Error While Creating $msg."]);
        }
    }

    public function show($id)
    {
        $id =  request()->get('id');
        $image = Image::where('id',$id)->first();
        $path = public_path('/uploads/images/'.$image->image);
        unlink($path);
        $image->delete();
        return response()->json(['msg' => 'success']);
    }

    public function edit($id)
    {
        $post = Category::where('id',$id)->first();
        $post->image = asset('/uploads/category/'.$post->image);
        $images = [];
        if($post->type == 'style'){
            $images = Image::where(['type' => 'style', 'post_id' => $id])->get();
        }
        elseif($post->type == 'portfolio'){
            $images = Image::where(['type' => 'portfolio', 'post_id' => $id])->get();
        }
        if(isset($post)){
            return response()->json(['msg' => 'success', 'post' => $post, 'images' => $images]);
        }
    }

    public function update(Request $req, $id)
    {
        $id = isset($id) ? $id : $req->id;
        $req->validate([
            'name' => ['required','max:255',Rule::unique('categories')->where(function ($query) use ($req,$id) {
                return $query->where('type', $req->type)->where('id','!=',$id); }) ],
            'description' => 'required|min:50|max:1000'
        ]);
        if($req->type == 'package'){
            $req->validate([
                'price' => 'required',
                'discount_price' => 'required'
            ]);
        }

        if($req->hasFile('image')){ $req->validate([ 'image' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg', ]); }

        $update['name'] = $req->name;
        $update['description'] = $req->description;
        $update['type'] = $req->type;
        if($req->type == 'package'){
            $update['price'] = $req->price;
            $update['discount_price'] = $req->discount_price;
        }
        $update['user'] = Auth::id();
        if($req->hasFile('image')){
            $file = $req->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/category'), $filename);
            $update['image'] = $filename;
        }
        $msg = '';
        if($req->type == 'style'){ $msg == 'Style'; }
        elseif($req->type == 'room'){ $msg == 'Room'; }
        elseif($req->type == 'package'){ $msg == 'Package'; }
        elseif($req->type == 'portfolio'){ $msg == 'Portfolio'; }
        else{ $msg == 'Product'; }
        if(Category::where('id',$id)->update($update)){

            if($req->type == 'style' || $req->type == 'portfolio'){
                if($req->style_images){
                    $files = $req->file('style_images');
                    foreach ($files as $item) {
                        $ext = $item->getClientOriginalExtension();
                        $name = $item->getFilename();
                        $name = md5(rand()).'.'.$ext;
                        $item->move(public_path('uploads/images'), $name);
                        Image::create([
                            'image' => $name,
                            'type' => $req->type,
                            'post_id' => $id
                        ]);
                    }
                }
            }

            return response()->json(['msg' => 'success', 'res' => "$msg Update Successfully"]);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => "Error while updating $msg"]);
        }
    }

    public function destroy($id)
    {
        $cat = Category::where('id',$id)->first();
        $msg = '';
        if($cat->type == 'style'){ $msg == 'Style'; }
        elseif($cat->type == 'room'){ $msg == 'Room'; }
        elseif($cat->type == 'package'){ $msg == 'Package'; }
        elseif($cat->type == 'Portfolio'){ $msg == 'Portfolio'; }
        else{ $msg == 'Product'; }
        if(Category::where('id',$id)->delete()){
            return response()->json(['msg' => 'success', 'res' => "$msg Deleted Successfully"]);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => "Error While Deleting $msg"]);
        }
    }
}
