<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
use App\Models\Profile;
use App\Models\Roles;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Notifications\Welcome;
use Carbon\Carbon;
use Crypt;
use Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $req){
        $attribute = $req->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:8',
            'phone' => 'required|numeric|unique:users,phone'
        ]);
        $role = '';
        if(request()->get('type') && !empty(request()->get('type'))){
            if(request()->get('type') == 'automotive' || request()->get('type') == 'private' || request()->get('type') == 'User'){
                $seg = request()->get('type');
                $role = Roles::where('name',$seg)->first();
            }
            else{
                return response()->json(['msg' => 'error', 'res' => 'Invalid registeration type']);
            }
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid registeration type']);
        }
        

        $token = Str::random(32);
        $attribute['status'] = 0;
        $attribute['user_type'] = 'User';
        $attribute['password'] = bcrypt($req->password);
        $attribute['remember_token'] = $token;
        $user = User::create($attribute);
        if($user){
            $user->roles()->attach($role);
            $profile = new Profile();
            $profile->user = $user->id;
            $profile->save();
            $email = $req->email;
            $name = $req->name;
            $data = [
                'name' => $name,
                'email' => $email,
                'url' => url('/email-verified/'.$token.'/'.$req->email),
                'msg' => 'Please click below button to verify your email',
            ];
            Mail::send('Mail.email-verified', ['data' => $data], function($message) use ($name,$email)
            {
                $message->to($email)->subject(env('APP_NAME').' Email Verification');
            });

            $when = Carbon::now()->addSeconds(10);
            $noti = Notification::route('mail',$email)->notify((new Welcome())->delay($when));

            return response()->json(['msg' => 'success', 'res' => 'An Email has Been sent to you with verification link Please check your mail.']);
            
        }

    }

    public function showRegistrationForm(){
        return view('auth.register',['type' => 'User']);
    }

    public function customRegister($type){
        $type == 'automotive' || $type == 'private' ? '' : abort(404);
        return view('auth.register',['type' => $type]);
    }

    public function customRegisterPost(Request $req, $type){
        $type == 'automotive' || $type == 'private' ? '' : abort(404);

        $attribute = $req->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:8',
            'phone' => 'required|numeric|max:20||unique:users,phone'
        ]);

        $attribute['type'] = $type;
        Session::put('registerData',$attribute);
        
        return response()->json(['msg' => 'success', 'res' => '']);
        

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'numeric'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return 1;
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);
    }
}
