<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\Str;
use Auth;
use Mail;
use Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $req){
        $req->validate([
            'email' => 'required|email',
        ]);

        if(User::where('email',$req->email)->exists()){
            if(PasswordReset::where('email',$req->email)->exists()){
                PasswordReset::where('email',$req->email)->delete();
            }
            $token = Str::random(32);
            $pass = new Password();
            $pass->email = $req->email;
            $pass->token = $token;
            if($pass->save()){
                $user = User::where('email',$req->email)->first();
                $email = $req->email;
                $data = [
                    'name' => $user->name,
                    'url' => url('/password/reset/'.$token.'?email='.$req->email),
                    'msg' => 'Please click below button to Update Your Password',
                ];

                Mail::send('Mail.email-verified', ['data' => $data], function($message) use ($email)
                {
                    $message->to($email)->subject(env('APP_Name').' Password Reset');
                });

                return response()->json(['msg' => 'success', 'Password Reset Link Sent To '.$req->email]);

            }
            else{
                return response()->json(['msg' => 'error', 'Unable To Generate Password Reset Link']);   
            }

        }
        else{
            return response()->json(['msg' => 'error', 'Email Not Found In Database']);
        }
    }

    public function showResetForm($token){
        $email = request()->get('email');
        if(PasswordReset::where('email',$email)->exists()){
            $tok = PasswordReset::where('email',$email)->first();
            if($tok->token == $token){
                return view('auth.passwords.reset',['email' => $email]);
            }
            else{
                Session::flush('message','Invalid Email Reset Token');
                Session::flush('alert','alert-danger');
                return redirect('/password-reset');
            }
        }
    }
}
