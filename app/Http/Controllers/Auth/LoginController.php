<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Mail;
use Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $req){
        $req->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $creds = ['email' => $req->email, 'password' => $req->password];
        
        if(Auth::attempt($creds)){
            if(Auth::user()->email_verified_at == null){
                $token = Str::random(32);
                $email = $req->email;
                User::where('id',Auth::id())->update(['remember_token' => $token]);
                $data = [
                    'name' => Auth::user()->name,
                    'url' => url('/email-verified/'.$token.'/'.$req->email),
                    'msg' => 'Please click below button to verify your email',
                ];

                Mail::send('Mail.email-verified', ['data' => $data], function($message) use ($email)
                {
                    $message->to($email)->subject(env('APP_Name').' Email Verification');
                });
                Auth::logout();
                Session::flush();
                return response()->json(['msg' => 'error', 'res' => 'A verification email has been sent to following email "'.$req->email.'" ']);
            }
            if(Auth::user()->status == 0){
                Auth::logout();
                Session::flush();
                return response()->json(['msg' => 'error', 'res' => 'Your account is disabled please contact admin to get it activated.']);
                
            }
            else{
            $res = url('/');
            if(Auth::user()->user_type == 'Admin'){
                $res = url('/admin/dashboard');
            }
            elseif(Auth::user()->user_type == 'User'){
                $res = url('/home');
            }
            elseif(Auth::user()->user_type == 'automotive'){
                $res = url('/automotive/dashboard');
            }
            elseif(Auth::user()->user_type == 'private'){
                $res = url('/private/dashboard');
            }
            return response()->json(['msg' => 'success', 'res' => 'Login Successfully.', 'url' => $res]);
            }
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid Email Or Password']);
        } 
    }
}
