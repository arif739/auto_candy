<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        // $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function emailVerified($token,$email){
        if(User::where(['email' => $email, 'remember_token' => $token])->exists()){
            User::where(['email' => $email])->update(['status' => 0, 'email_verified_at' => now(), 'remember_token' => '']);
            Session::flash('message','Email successfully verified. Admin approval is pending');
            Session::flash('alert','alert-success');
            return redirect('/login');
        }
        else{
            Session::flash('message','Email verification failed');
            Session::flash('alert','alert-danger');
            return redirect('/login');
        }
    }
}
