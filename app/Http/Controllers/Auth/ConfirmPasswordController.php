<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use App\Models\User;

class ConfirmPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Confirm Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password confirmations and
    | uses a simple trait to include the behavior. You're free to explore
    | this trait and override any functions that require customization.
    |
    */

    use ConfirmsPasswords;

    /**
     * Where to redirect users when the intended url fails.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function confirm(Request $req){
        $req->validate([
            'password' => 'required|min:6|string',
        ]);

        $password = bcrypt($req->password);
        if(User::where('email',$req->email)->update(['password' => $password])){
            return response()->json(['msg' => 'success', 'res' => 'Password update successfully', 'url' => url('/login')]);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while updating Password']);
        }
    }
}
