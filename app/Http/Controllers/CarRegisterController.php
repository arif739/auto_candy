<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CarOption;
use App\Models\CarRegister;
use App\Models\CarRegisterUserDetail;
use App\Models\User;
use App\Notifications\NewCar;
use Illuminate\Support\Facades\Notification;
use Auth;

class CarRegisterController extends Controller
{
    public function index(){
        if(Auth::user()->hasPackage()){
            if(!Auth::user()->isPackageExpire()){
                if(Auth::user()->vehicleLeft()){
                    $cars = CarOption::get();
                    return view('auto-candy.car-form',['cars' => $cars, 'type' => 'continue', 'states' => User::getStates()]);
                }
                else{
                    return view('auto-candy.car-form',['msg' => 'No vehical left in current package please upgrade your package. Below are details for current package.', 'pkg' => Auth::user()->packageDetails(), 'type' => 'no-vehicle']);
                }
            }
            else{
                return view('auto-candy.car-form',['msg' => 'Package expire please upgrade your package. Below are details for current package.', 'pkg' => Auth::user()->packageDetails(), 'type' => 'expire']);
            }
        }
        else{
            return view('auto-candy.car-form',['msg' => 'Please purchase a package before submitting car information. Click below button to check out packages', 'type' => 'no-package']);

        }
    }

    public function carRegisterPost(Request $req){
            $attributes = $req->validate([
                'vin_number' => 'required|max:190',
                'price' => 'required',
                'make' => 'required|not_in:0',
                'model' => 'required|min:2|max:190',
                'body_style' => 'required|not_in:0',
                'year' => 'required|max:190',
                'condition' => 'required|not_in:0',
                'mileage' => 'required',
                'transmission' => 'required|not_in:0',
                'drivetrain' => 'required|not_in:0',
                'engine' => 'required|min:2|max:190',
                'fuel' => 'required|not_in:0',
                'fuel_economy' => 'required|not_in:0',
                'trim' => 'required|min:2|max:190',
                'exterior_color' => 'required|min:2|max:190',
                'interior_color' => 'required|min:2|max:190',
                'stock_number' => 'required|min:2|max:190',
                'status' => 'required|not_in:0',
                'feature_option' => 'required',
            ]);

            $req->validate([
                'name' => 'required|min:2|max:190',
                'email' => 'required|email',
                'phone' => 'required|numeric',
                'zip' => 'required|min:4|max:190',
                'address' => 'required',
                'city' => 'required|min:2|max:190',
                'state' => 'required|not_in:0',
            ]);

            $attributes['vin'] = $req->vin_number;
            $attributes['car_status'] = $req->status;
            $attributes['status'] = 0;
            $attributes['dealer'] = Auth::user()->user_type;
            $attributes['user'] = Auth::id();
            $car = CarRegister::create($attributes);
            if($car){
                $id = $car->id;
                $user = new CarRegisteruserDetail();
                $user->user = Auth::id();
                $user->car_register_id = $id;
                $user->name = $req->name;
                $user->email = $req->email;
                $user->phone = $req->phone;
                $user->zip = $req->zip;
                $user->city = $req->city;
                $user->state = $req->state;
                $user->save();

                $email = $req->email;
                $adminMsg = "New Car Detail Has Been Submitted By {$req->name} email {$req->email}";
                $userMsg = "Thank you for submiting car details autocandy team will review and process it further.";

                $when = Carbon::now()->addSeconds(5);
                Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify((new NewCar($adminMsg))->delay($when));

                $userWhen = Carbon::now()->addSeconds(10);
                Notification::route('mail',$email)->notify((new NewCar($userMsg))->delay($userWhen));
                
                return response()->json(['msg' => 'success', 'res' => 'Thank You! car info has been submitted successfully after autocandy team will review and process it further']);
                
            }
            

    }
}
