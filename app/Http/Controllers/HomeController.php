<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Profile;
use App\Models\Package;
use App\Models\Coupon;
use App\Models\UserPackages;
use App\Models\CheckOut;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Image;
use Stripe;
use Auth;
use Crypt;
use Mail;
use Session;

class HomeController extends Controller
{
    public function index(){
        return view('auto-candy.home');
    }

    public function package($type){
        $type == 'automotive' || $type == 'private' ? '' : abort(404);
        $packages = Package::where(['type' => $type])->orderBy('order')->get();
        return $type == 'automotive' ? view('auto-candy.automotive-package',['packages' => $packages]) : view('auto-candy.private-package',['packages' => $packages]);
    }

    public function sellingProfile(){
        return view('auto-candy.selling-profile');
    }

    public function products(){
        $sub = request()->get('sub');
        $cat = request()->get('cat');
        if(isset($cat)){
            $category = Category::where(['name' => $cat, 'type' => 'product'])->first();
            $products = Product::where(['type' => 'product', 'category' => $category->id])->paginate(20);
        }
        elseif(isset($sub)){
            $category = SubCategory::where(['name' => $sub, 'type' => 'product'])->first();
            $products = Product::where(['type' => 'product', 'sub_category' => $category->id])->paginate(20);
        }
        else{
            $products = Product::where('type','product')->paginate(20);
        }
        $categories = Category::where('type','product')->get();
        $subCategories = SubCategory::where('type','product')->get();
        return view('auto-candy.product',['products' => $products, 'categories' => $categories, 'subCategories' => $subCategories]);
    }

    public function productDetails($id){
        $id = Crypt::decrypt($id);
        $product = Product::where(['type' => 'product', 'id' => $id])->first();
        $categories = Category::where('type','product')->get();
        $subCategories = SubCategory::where('type','product')->get();
        $images = Image::where(['post_id' => $product->id, 'type' => 'product'])->get();
        $similar = Product::where(['type' => 'product', 'category' => $product->id])->where('id','!=',$product->id)->limit(5)->get();
        return view('auto-candy.product-details',['product' => $product, 'images' => $images, 'categories' => $categories, 'subCategories' => $subCategories, 'similar' => $similar]);
    }

    public function checkoutPackage($id){
        session()->forget('coupon');
        $id = Crypt::decrypt($id);
        $from = Carbon::now();
        if(Package::where('id',$id)->exists()){
            $pkg = Package::where('id',$id)->first();
            if($pkg->package_type == 'Paid'){
                return view('auto-candy.check-out',['pkg' => $pkg]);
            }
            else{
                if(auth()->user()->is_package_free_trial == 1){
                    session()->flash('message','You\'ve already avail '.$pkg->title);
                    session()->flash('alert','alert-success');
                    return redirect()->back(); 
                }
                $to = $from->addDays($pkg->trial_days);
                $userPkg = new UserPackages();
                $userPkg->user_id = Auth::id();
                $userPkg->package_id = $pkg->id;
                $userPkg->package_type = $pkg->package_type;
                $userPkg->package_days = $pkg->trial_days;
                $userPkg->package_price = $pkg->price;
                $userPkg->package_expiry_date = $to;
                if($userPkg->save()){
                    auth()->user()->is_package_free_trial = 1;
                    auth()->user()->save();
                    session()->flash('message',$pkg->title.' has been activated and valid till '.Carbon::parse($to)->format('M d Y'));
                    session()->flash('alert','alert-success');
                    return redirect()->back();
                }

            }
            
        }
        else{
            session()->flash('message','Invalid package');
            session()->flash('alert','alert-danger');
            return redirect()->back();
        }
        
    }

    public function checkoutPost(Request $req){
        $req->validate([
            'name' => 'required|max:190',
            'email' => 'required|email|max:190',
            'address' => 'required|max:190'
        ]);
        
        $total = 0.00;
        $isCoupon = false;
        $coupon = '';
        $id = Crypt::decrypt($req->package);
        $pkg = Package::where('id',$id)->first();
        if(Session::has('coupon')){
            $cou = Session::get('coupon');
            $total = $cou['after_discount_price'];
            $isCoupon = true;
            $coup = Coupon::where('code',$cou['coupon_code'])->first();
            $coupon = $coup->id;
        }
        else{
            $total = $pkg->price;
            $isCoupon = false;
        }

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => round((float)$total,0)*100,
                "currency" => "usd",
                "source" => $req->stripeToken,
                "description" => "Auto Candy $pkg->title Package Purchase"
        ]);

        $order = new CheckOut();
        $order->name = $req->name;
        $order->email = $req->email;
        $order->address = $req->address;
        $order->is_coupon = $isCoupon;
        $order->total = $total;
        $order->after_discount = $isCoupon ? $total : null;
        $order->coupon_id = $isCoupon ? $coupon : null;
        $order->order_status = 1;
        $order->user_id = Auth::id();
        $order->package_id = $pkg->id;
        $order->type = 'package';
        if($order->save()){
            $date = null;
            if($pkg->duration == 2){
                $date = Carbon::now()->addMonths(1);
            }
            $userPkg = new UserPackages();
            $userPkg->user_id = Auth::id();
            $userPkg->package_id = $pkg->id;
            $userPkg->package_type = $pkg->package_type;
            $userPkg->package_days = $pkg->trial_days;
            $userPkg->package_price = $pkg->price;
            $userPkg->package_expiry_date = $date;
            $userPkg->save();
            session()->forget('coupon');
            $data = [
                'name' => $req->name,
                'email' => $req->email,
                'price' => $total,
                'title' => $pkg->title,
            ];

            Mail::send('Mail.package-purchased', ['data' => $data], function($message)
            {
                $message->to(env('MAIL_FROM_ADDRESS'))->subject(env('APP_NAME').' Package Purchased');
            });

            return response()->json(['msg' => 'success', 'res' => 'Payment Successfull. '.$pkg->title.' successfully purchased' ]);
        }

    }

    public function couponRedeem($val,$id){
        if(Coupon::where('code',$val)->exists()){
            $cou = Coupon::where('code',$val)->first();
            $date = new Carbon;
            if($date > $cou->expiry_date){
                return response()->json(['msg' => 'error', 'res' => 'Coupon Code Expired.']);
            }
            else{
                $id = Crypt::decrypt($id);
                $pkg = Package::where('id',$id)->first();
                $subtotal = $pkg->price;
                $total = 0.00;
                if($cou->coupon_type == 'Percent'){
                    $percent = $subtotal / (int)$cou->value;
                    $total = $subtotal - $percent;
                }
                else{
                    $total = $subtotal - (int)$cou->value;
                }
                $arr = [
                    'coupon_code' => $val,
                    'discount_type' => $cou->coupon_type,
                    'discount' => $cou->value,
                    'after_discount_price' => $total,
                    'orignal_total' => $subtotal
                ];
                Session::put('isCoupon',true);
                Session::put('coupon',$arr);
                $type = $cou->coupon_type == 'Percent' ? $cou->value.'%' : '$'.$cou->value;
                return response()->json(['msg' => 'success', 'title' => $cou->title, 'dis' => $type, 'total' => $total]);
            }
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid Coupon Code']);
        }
    }


    public function couponRedeemProduct($val){
        if(Coupon::where('code',$val)->exists()){
            $cou = Coupon::where('code',$val)->first();
            $date = new Carbon;
            if($date > $cou->expiry_date){
                return response()->json(['msg' => 'error', 'res' => 'Coupon Code Expired.']);
            }
            else{
                $subtotal = 0.0;
                $total = 0.00;
                foreach(Session::get('cart') as $cart){
                    $subtotal += (int)$cart['total'];
                }
                
                if($cou->coupon_type == 'Percent'){
                    $percent = $subtotal / (int)$cou->value;
                    $total = $subtotal - $percent;
                }
                else{
                    $total = $subtotal - (int)$cou->value;
                }
                $arr = [
                    'coupon_code' => $val,
                    'discount_type' => $cou->coupon_type,
                    'discount' => $cou->value,
                    'after_discount_price' => $total,
                    'orignal_total' => $subtotal
                ];
                Session::put('isCoupon',true);
                Session::put('coupon',$arr);
                $type = $cou->coupon_type == 'Percent' ? $cou->value.'%' : '$'.$cou->value;
                return response()->json(['msg' => 'success', 'title' => $cou->title, 'dis' => $type, 'total' => $total]);
            }
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Invalid Coupon Code']);
        }
    }

    public function profile($id = null){
        $states = array('AL' => "Alabama", 'AK' => "Alaska", 'AZ' => "Arizona", 'AR' => "Arkansas", 'CA' => "California", 'CO' => "Colorado",'CT' => "Connecticut",
        'DE' => "Delaware", 'DC' => "District Of Columbia", 'FL' => "Florida", 'GA' => "Georgia", 'HI' => "Hawaii", 'ID' => "Idaho", 'IL' => "Illinois", 'IN' => "Indiana",
        'IA' => "Iowa",'KS' => "Kansas",'KY' => "Kentucky",'LA' => "Louisiana",'ME' => "Maine",'MD' => "Maryland",'MA' => "Massachusetts",'MI' => "Michigan",'MN' => "Minnesota",'MS' => "Mississippi",
        'MO' => "Missouri",'MT' => "Montana",'NE' => "Nebraska",'NV' => "Nevada",'NH' => "New Hampshire",'NJ' => "New Jersey",'NM' => "New Mexico",'NY' => "New York",'NC' => "North Carolina",
        'ND' => "North Dakota",'OH' => "Ohio",'OK' => "Oklahoma",'OR' => "Oregon",'PA' => "Pennsylvania",'RI' => "Rhode Island",'SC' => "South Carolina",'SD' => "South Dakota",'TN' => "Tennessee",
        'TX' => "Texas",'UT' => "Utah",'VT' => "Vermont",'VA' => "Virginia",'WA' => "Washington",'WV' => "West Virginia",'WI' => "Wisconsin",'WY' => "Wyoming");

        if($id){
            $id = Crypt::decrypt($id);
            $data = User::where('id',Auth::id())->first();
            $user = Profile::where('user',$id)->first();
            return view('profile.show',['user' => $user, 'data' => $data, 'states' => $states]);
        }
        else{
            $data = User::where('id',Auth::id())->first();
            $user = Profile::where('user',Auth::id())->first();
            return view('profile.show',['user' => $user, 'data' => $data, 'states' => $states]);
        }
    }

    public function profileUpdate(Request $req){
        $validate = [
            'name' => 'required|string|min:3|max:50',
            'phone' => 'required|numeric|unique:users,phone,'.Auth::id().',id',
            'work_phone_number' => 'required|numeric|unique:profiles,work_phone_number,'.Auth::id().',user',
            'preferred_contact_method' => 'required|not_in:0',
            'address' => 'required|max:150',
            'city' => 'required|max:150',
            'state' => 'required|not_in:0',
            'zip' => 'required|max:150',
        ];
        if(Auth::user()->user_type == 'automotive'){
            $validate['dealership_name'] = 'required|string|min:3|max:50';
        }
        else{
            if(Auth::user()->profile()->username == '' || Auth::user()->profile()->username == null){
                $validate['fan_name'] = 'required|min:3|max:190|unique:profiles,username,'.Auth::id().',user';
            }
            $validate['favorite_vehicle'] = 'required|string|min:3|max:50';
        }

        if(Auth::user()->profile()->image == '' || Auth::user()->profile()->image == null){
            $validate['profileImage'] = 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg';
        }
        $req->validate($validate);
        
        $user['name'] = $req->name;
        $user['phone'] = $req->phone;
        $profile = [];

        if($req->work_phone_number){
            $profile['work_phone_number'] = $req->work_phone_number;
        }

        if($req->fan_name){
            $profile['username'] = $req->fan_name;
        }

        if($req->favorite_vehicle){
            $profile['favorite_car'] = $req->favorite_vehicle;
        }

        if($req->dealership_name){
            $profile['dealership_name'] = $req->dealership_name;
        }

        if($req->preferred_contact_method){
            $profile['preferred_contact_method'] = $req->preferred_contact_method;
        }

        if($req->address){ 
            $req->validate([ 'address' => 'required|max:150' ]);
            $profile['address'] = $req->address;
        }
        if($req->city){ 
            $req->validate([ 'city' => 'required|max:150' ]);
            $profile['city'] = $req->city;
        }
        if($req->state){ 
            $req->validate([ 'state' => 'required|max:150' ]);
            $profile['state'] = $req->state;
        }
        if($req->zip){ 
            $req->validate([ 'zip' => 'required|max:150' ]);
            $profile['zip'] = $req->zip;
        }
        if($req->skype){ 
            $req->validate([ 'skype' => 'required|max:150' ]);
            $profile['skype'] = $req->skype;
        }
        if($req->facebook){ 
            $req->validate([ 'facebook' => 'required|max:190' ]);
            $profile['facebook'] = $req->facebook;
        }
        if($req->twitter){ 
            $req->validate([ 'twitter' => 'required|max:190' ]);
            $profile['twitter'] = $req->twitter;
        }
        if($req->aboutMe){ 
            $req->validate([ 'aboutMe' => 'required|max:500' ]);
            $profile['bio'] = $req->aboutMe;
        }
        if($req->hasFile('profileImage')){ 
            $req->validate([ 'profileImage' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,svg' ]);
            $file = $req->file('profileImage');
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename();
            $filename = md5(rand()).'.'.$extension;
            $file->move(public_path('uploads/profile'), $filename);
            $profile['image'] = $filename;
        }

        if(User::where('id',Auth::id())->update($user)){
            Profile::where('user',Auth::id())->update($profile);
            return response()->json(['msg' => 'success', 'res' => 'Profile update successfully']);
        }

    }

    public function updatePassword(Request $req){
        $req->validate([
            'currentPassword' => 'required',
            'newPassword' => 'required|different:currentPassword|string|min:6',
        ]);

        $user = User::findOrFail(Auth::id());

        if (Hash::check($req->currentPassword, $user->password)) { 
            $user->fill([
             'password' => Hash::make($req->newPassword)
             ])->save();
             return response()->json(['msg' => 'success', 'res' => 'Password update successfully']);
         }
         else{
            return response()->json(['msg' => 'error', 'res' => 'Password update successfully']);
         }
    }
}
