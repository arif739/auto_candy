<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Models\Product;
use App\Models\UserOrder;
use App\Models\UserOrderDetails;
use App\Models\Coupon;
use App\Models\User;
use App\Notifications\NewOrder;
use Carbon\Carbon;
use Auth;
use Session;
use Stripe;

class CartController extends Controller
{
    public function index()
    {
        return view('auto-candy.cart',['cart' => Session::has('cart') ? Session::get('cart') : []]);
    }

    public function create()
    {
        //
    }

    public function store(Request $req)
    {
        $id = $req->id;
        $product = Product::with(['categories'])->where(['id' => $id])->first();
        $arr = [];
        $data = [
            'id' => $id,
            'price' => $product->price,
            'total' => $product->price,
            'quantity' => $req->quant,
            'name' => $product->name,
            'description' => $product->short_description,
            'img' => asset('uploads/product'.'/'.$product->image),
        ];
        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach (Session::get('cart') as $key=>$item) {
                if($item['id'] == $id){
                    unset($cart[$key]);
                }
                else{
                    $array = [
                        'id' => $item['id'],
                        'price' => $item['price'],
                        'total' => $item['total'],
                        'quantity' => $item['quantity'],
                        'name' => $item['name'],
                        'description' => $item['description'],
                        'img' => $item['img'],
                    ];
                    array_push($arr,$array);
                }
            }
            array_push($arr,$data);
            Session::put('cart',$arr);
        }
        else{
            array_push($arr,$data);
            Session::put('cart',$arr);
        }
        
        return response()->json(['msg' => 'success', 'cartCount' => count(Session::get('cart'))]);
        

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $req, $id)
    {
        $id = isset($id) ? $id : $req->id;
        $product = Product::with(['categories'])->where(['id' => $id])->first();
        if((int)$req->quantity < (int)$product->min_quantity){
            return response()->json(['msg' => 'error', 'res' => 'Minimum quantity for this product is '.$product->min_quantity, 'val' => $product->min_quantity ]);
        }
        else if((int)$req->quantity > (int)$product->max_quantity){
            return response()->json(['msg' => 'error', 'res' => 'Maximum quantity for this product is '.$product->max_quantity, 'val' => $product->max_quantity ]);
        }
        else{
        $arr = [];
        $sub = (int)$req->quantity * (float)$product->price;
        $data = [
            'id' => $id,
            'total' => (int)$req->quantity * (float)$product->price,
            'price' => $product->price,
            'quantity' => $req->quantity,
            'name' => $product->name,
            'description' => $product->description,
            'img' => asset('uploads/product/'.$product->image),
        ];
        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach (Session::get('cart') as $key=>$item) {
                if($item['id'] == $id){
                    unset($cart[$key]);
                }
                else{
                    $array = [
                        'id' => $item['id'],
                        'total' => $item['total'],
                        'price' => $item['price'],
                        'quantity' => $item['quantity'],
                        'name' => $item['name'],
                        'description' => $item['description'],
                        'img' => $item['img'],
                    ];
                    array_push($arr,$array);
                }
            }
            array_push($arr,$data);
            Session::put('cart',$arr);
        }
        $tot = 0;
        foreach (Session::get('cart') as $cart) {
            $tot += (float)$cart['total'];
        }

            return response()->json(['msg' => 'success', 'total' => number_format(round($tot,2)), 'data' => $data, 'sub' => $sub, 'cartCount' => count(Session::get('cart'))]);

        }
    }

    public function destroy($id)
    {
        $arr = [];
        $product = Product::with(['categories'])->where(['id' => $id])->first();
        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach (Session::get('cart') as $key=>$item) {
                if($item['id'] == $id){
                    unset($cart[$key]);
                }
                else{
                    $array = [
                        'id' => $item['id'],
                        'total' => $item['total'],
                        'price' => $item['price'],
                        'quantity' => $item['quantity'],
                        'name' => $item['name'],
                        'description' => $item['description'],
                        'img' => $item['img'],
                    ];
                    array_push($arr,$array);
                }
            }
            Session::put('cart',$arr);
        }
        $total = 0;
        foreach (Session::get('cart') as $cart) {
            $total += (float)$cart['total'];
        }
        return response()->json(['msg' => 'success', 'total' => round($total,2), 'cartCount' => count(Session::get('cart'))]);
    }


    public function checkout(){
        session()->forget('coupon');
        $cart = Session::has('cart') ? Session::get('cart') : [];
        if(count($cart) > 0){
            if(Auth::check()){
                $states = User::getStates();
                return view('auto-candy.checkout-store', ['cartData' => $cart, 'count' => count($cart), 'states' => $states]);
            }
            else{
                Session::flash('message','Please Login Or Create An Account To Continue');
                Session::flash('alert','alert-info');
                return redirect('/login');
            }
        }
        else{
            return redirect('/products');
        }

    }

    public function checkoutStore(Request $req){
        $req->validate([
            'name' => 'required|min:3|max:90',
            'email' => 'required|email',
            'address' => 'required|max:250',
            'country' => 'required|not_in:0',
            'state' => 'required|not_in:0',
            'zip' => 'required|not_in:0',
        ]);
        $total = 0.00;
        $isCoupon = false;
        $coupon = '';
        if(Session::has('coupon')){
            $cou = Session::get('coupon');
            $total = $cou['after_discount_price'];
            $isCoupon = true;
            $coup = Coupon::where('code',$cou['coupon_code'])->first();
            $coupon = $coup->id;
        }
        else{
            $cartData = Session::get('cart');
            foreach($cartData as $cart){
                $total += (float)$cart['total'];
            }
        }

        try {
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe = Stripe\Charge::create ([
                    "amount" => round((float)$total,0)*100,
                    "currency" => "usd",
                    "source" => $req->stripeToken,
                    "description" => env('APP_NAME')." Store Purchase"
            ]);
        } catch (\Exception $th) {
            dd($th);
        }

        $order = new UserOrder();
        $order->name = $req->name;
        $order->email = $req->email;
        $order->address = $req->address;
        $order->address2 = $req->address2 ? $req->address2 : '-';
        $order->country = $req->country;
        $order->state = $req->state;
        $order->zip = $req->zip;
        $order->is_coupon = $isCoupon;
        $order->total = $total;
        $order->after_discount = $isCoupon ? $total : null;
        $order->coupon_id = $coupon ? $coupon : null;
        $order->order_status = 0;
        $order->user = Auth::id();
        if($order->save()){
            $order_id = $order->id;
            $checkout = Session::get('cart');
            foreach($checkout as $item){
                UserOrderDetails::create([
                    'order_id' => $order_id,
                    'user_id' => Auth::id(),
                    'product_id' => $item['id'],
                    'total' => $item['total'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                ]);
            }
            Session::forget('coupon');
            Session::forget('cart');

            $name = $req->name;
            $email = $req->email;

            $when = Carbon::now()->addSeconds(10);
            $noti = Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify((new NewOrder($name,$email))->delay($when));
            return response()->json(['msg' => 'success', 'res' => 'Order Placed Successfully. order is pending for approvel once its approve you will be notify through email.']);
        }
        else{
            return response()->json(['msg' => 'error', 'res' => 'Error while placing order']);
        }

    }
}
