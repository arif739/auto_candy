<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckOut extends Model
{
    use HasFactory;

    
    public function packages()
    {
        return $this->belongsTo('App\Models\Package', 'package_id');
    }
    
    
    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    
}
