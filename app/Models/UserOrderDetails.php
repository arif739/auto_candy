<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOrderDetails extends Model
{
    use HasFactory;

    protected $fillable = [ 'order_id', 'user_id', 'product_id', 'total', 'price', 'quantity' ];

    
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
}
