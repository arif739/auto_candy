<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category');
    }

    
    public function subCategories()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_category');
    }
    
    
}
