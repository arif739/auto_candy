<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Profile;
use App\Models\UserPackages;
use App\Models\CarRegister;
use Carbon\Carbon;
use Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function profile()
    {
        return Profile::where('user',$this->id)->first();
    }
    
    

    public function roles(){
        return $this->belongsToMany('App\Models\Roles', 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles){
        if(is_array($roles)){
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        }
        else{
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){
        if($this->roles()->where('name', $role)->first()){
            return true;
        }
        return false;
    }

    public function hasPackage(){
        $pkg = UserPackages::with(['users','package'])->where('user_id',Auth::id())->orderBy('id','DESC')->first();
        return isset($pkg) ? true : false;
    }

    public function packageDetails(){
        $pkg = UserPackages::with(['users','package'])->where('user_id',Auth::id())->orderBy('id','DESC')->first();
        return isset($pkg) ? $pkg : false;
    }

    public function isPackageExpire(){
        $pkg = UserPackages::with(['users','package'])->where('user_id',Auth::id())->orderBy('id','DESC')->first();
        return $pkg->package_expiry_date <= Carbon::now() ? true : false;
    }

    public function vehicleLeft(){
        $pkg = UserPackages::with(['users','package'])->where('user_id',Auth::id())->orderBy('id','DESC')->first();
        $car = CarRegister::where('user',Auth::id())->where('status','!=',2)->count();
        return $pkg->package->vehicle >= $car ? true : false;
    }

    public function getUserStateName(){
        $states = array('AL' => "Alabama", 'AK' => "Alaska", 'AZ' => "Arizona", 'AR' => "Arkansas", 'CA' => "California", 'CO' => "Colorado",'CT' => "Connecticut",
        'DE' => "Delaware", 'DC' => "District Of Columbia", 'FL' => "Florida", 'GA' => "Georgia", 'HI' => "Hawaii", 'ID' => "Idaho", 'IL' => "Illinois", 'IN' => "Indiana",
        'IA' => "Iowa",'KS' => "Kansas",'KY' => "Kentucky",'LA' => "Louisiana",'ME' => "Maine",'MD' => "Maryland",'MA' => "Massachusetts",'MI' => "Michigan",'MN' => "Minnesota",'MS' => "Mississippi",
        'MO' => "Missouri",'MT' => "Montana",'NE' => "Nebraska",'NV' => "Nevada",'NH' => "New Hampshire",'NJ' => "New Jersey",'NM' => "New Mexico",'NY' => "New York",'NC' => "North Carolina",
        'ND' => "North Dakota",'OH' => "Ohio",'OK' => "Oklahoma",'OR' => "Oregon",'PA' => "Pennsylvania",'RI' => "Rhode Island",'SC' => "South Carolina",'SD' => "South Dakota",'TN' => "Tennessee",
        'TX' => "Texas",'UT' => "Utah",'VT' => "Vermont",'VA' => "Virginia",'WA' => "Washington",'WV' => "West Virginia",'WI' => "Wisconsin",'WY' => "Wyoming");
        return $states[$this->profile()->state];
    }

    public function isAutomotiveProfileComplete(){
        $pro = $this->profile();
        if($pro->address == '' || $pro->image == '' || $pro->city == '' || $pro->zip == '' || $pro->state == '' || $pro->dealership_name == '' || $pro->work_phone_number == '' || $pro->preferred_contact_method == ''){
            return false;
        }
        else{
            return true;
        }
    }

    public function isProfileComplete(){
        $pro = $this->profile();
        if($pro->address == '' || $pro->image == '' || $pro->city == '' || $pro->zip == '' || $pro->state == '' || $pro->username == '' || $pro->favorite_car == '' || $pro->work_phone_number == '' || $pro->preferred_contact_method == ''){
            return false;
        }
        else{
            return true;
        }
    }

    public static function getStates(){
        $states = array('AL' => "Alabama", 'AK' => "Alaska", 'AZ' => "Arizona", 'AR' => "Arkansas", 'CA' => "California", 'CO' => "Colorado",'CT' => "Connecticut",
        'DE' => "Delaware", 'DC' => "District Of Columbia", 'FL' => "Florida", 'GA' => "Georgia", 'HI' => "Hawaii", 'ID' => "Idaho", 'IL' => "Illinois", 'IN' => "Indiana",
        'IA' => "Iowa",'KS' => "Kansas",'KY' => "Kentucky",'LA' => "Louisiana",'ME' => "Maine",'MD' => "Maryland",'MA' => "Massachusetts",'MI' => "Michigan",'MN' => "Minnesota",'MS' => "Mississippi",
        'MO' => "Missouri",'MT' => "Montana",'NE' => "Nebraska",'NV' => "Nevada",'NH' => "New Hampshire",'NJ' => "New Jersey",'NM' => "New Mexico",'NY' => "New York",'NC' => "North Carolina",
        'ND' => "North Dakota",'OH' => "Ohio",'OK' => "Oklahoma",'OR' => "Oregon",'PA' => "Pennsylvania",'RI' => "Rhode Island",'SC' => "South Carolina",'SD' => "South Dakota",'TN' => "Tennessee",
        'TX' => "Texas",'UT' => "Utah",'VT' => "Vermont",'VA' => "Virginia",'WA' => "Washington",'WV' => "West Virginia",'WI' => "Wisconsin",'WY' => "Wyoming");
        return $states;
    }
}
